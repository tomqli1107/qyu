package main

import (
	"fmt"
	"log"
	"net/http"
	"golang.org/x/net/websocket"
)

func Echo(ws *websocket.Conn) {
	defer ws.Close()

	var err error

	for {
		var reply string
		if err = websocket.Message.Receive(ws, &reply); err != nil {
			fmt.Println("Can't receive" + err.Error())
			break
		}

		if reply == "quit" {
			break
		}

		fmt.Println("Received back from client: " + reply)

		msg := "Received:  " + reply
		fmt.Println("Sending to client: " + msg)

		if err = websocket.Message.Send(ws, msg); err != nil {
			fmt.Println("Can't send")
			break
		}
	}
}

func main() {
	http.Handle("/", websocket.Handler(Echo))

	fmt.Println("Listen: 192.168.1.102:20001")

	if err := http.ListenAndServe("0.0.0.0:20001", nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
