<?php
namespace console\controllers;

use Yii;
use console\components\Controller;

class SmsController extends Controller {

    public function actionSendCode($phoneNum = '', $code = '') {
        try {
            Yii::$app->sms->sendCode($phoneNum, $code);
        } catch (\Exception $ex) {
            Yii::error("Send SMS Code Error[{$phoneNum} {$code}]: ", $ex->getMessage());
        }
    }
}