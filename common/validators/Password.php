<?php
/**
 * 密码验证器
 */
namespace common\validators;

use common\components\C;
use common\components\Validator;

class Password extends Validator {

    public $type;
    public $skipOnEmpty = false;

    public function validateAttribute($model, $attribute) {
        $model->{$attribute} = trim($model->{$attribute});
        $password = $model->{$attribute};

        if ($this->scene == C::SCENE_REG || ($this->scene==C::SCENE_LOGIN&&$this->type==C::LOGIN_TYPE_PW)) {
            if (!$password) {
                $this->addError($model, $attribute, '请输入密码');
            } else if (!preg_match('/^\S{6,18}$/', $password)) {
                $this->addError($model, $attribute, '密码格式不正确');
            }
        }
    }
}