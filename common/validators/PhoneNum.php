<?php
/**
 * 手机号码验证器
 */
namespace common\validators;

use common\models\User;
use common\components\C;
use common\components\Validator;

class PhoneNum extends Validator {

    public $skipOnEmpty = false;

    public function validateAttribute($model, $attribute) {
        $model->{$attribute} = trim($model->{$attribute});
        $phoneNum = $model->{$attribute};
        if (!$phoneNum) {
            $this->addError($model, $attribute, '请输入手机号码');
        } else if (!preg_match('/^1[0-9]{10}$/', $phoneNum)) {
            $this->addError($model, $attribute, '无效的手机号码');
        } else {
            switch ($this->scene) {
                case C::SCENE_REG:
                    /* 判断该手机号码是否已经注册 */
                    if (User::existByPhone($phoneNum)) {
                        $this->addError($model, $attribute, '该手机号码已被注册');
                    }
                    break;
            }
        }
    }
}