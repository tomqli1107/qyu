<?php
/**
 * 短信验证码验证器
 */
namespace common\validators;

use Yii;
use common\components\C;
use common\components\Validator;

class SmsCode extends Validator {

    public $type;
    public $phoneNum;
    public $skipOnEmpty = false;

    public function validateAttribute($model, $attribute) {
        $model->{$attribute} = trim($model->{$attribute});

        if ($this->scene == C::SCENE_REG || ($this->scene==C::SCENE_LOGIN&&$this->type==C::LOGIN_TYPE_CODE)) {
            if (!$model->{$attribute}) {
                $this->addError($model, $attribute, '请输入验证码');
            }
            if (!Yii::$app->sms->verifyCode($this->phoneNum, $model->{$attribute})) {
                $this->addError($model, $attribute, '错误的验证码');
            }
        }
    }
}