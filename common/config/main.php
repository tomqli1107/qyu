<?php

$aliyun = require(__DIR__ . '/aliyun.php');

return [
    'vendorPath'    => dirname(dirname(__DIR__)) . '/vendor',
    'aliases'       => [
        '@vendor' => dirname(dirname(__DIR__)) . '/vendor',
    ],
    'components'    => [
        'chatLog' => [
            'class' => 'common\components\ChatLog',
            'db'    => 'logDb',
        ],
        'cache' => [
            'class'         => 'common\components\RedisCache',
            'host'          => '120.78.196.38',
            'port'          => 6379,
            'database'      => 0,
            //'password'      => 'tomqli1107Q',
            'password'      => 'M3yRRyXtUd82nFAA',
            'persistent'    => false
        ],
        'aliyunMNS' => [
            'class'             => 'common\components\ali\AliyunMNS',
            'accessId'          => $aliyun['accessId'],
            'accessKey'         => $aliyun['accessKey'],
            'endpoint'          => $aliyun['sms']['endpoint'],
            'smsTopicName'      => $aliyun['sms']['topicName'],
            'smsSignName'       => $aliyun['sms']['signName'],
            'smsTemplateCode'   => $aliyun['sms']['templateCode'],
        ],
        'aliyunOSS' => [
            'class'             => 'common\components\ali\AliyunOSS',
            'accessId'          => $aliyun['accessId'],
            'accessKey'         => $aliyun['accessKey'],
            'endpoint'          => $aliyun['oss']['endpoint'],
            'bucket'            => $aliyun['oss']['bucket'],
            'avatarDir'         => $aliyun['oss']['avatarDir'],
            'suffix'            => '.png',
        ],
        'sms' => [
            'class'     => 'common\components\Sms',
            'driver'    => 'aliyunMNS',
            'codeLen'   => 4,
            'duration'  => 300,
        ],
        'taskQueue' => [
            'class'     => 'common\components\TaskQueue',
            'driver'    => 'cache',
        ],
        'log' => [
            'traceLevel'=> YII_DEBUG ? 3 : 0,
            'targets'   => [
                [
                    'class'     => 'common\components\LogDbTarget',
                    'levels'    => ['error', 'warning'],
                    'enabled'   => true,
                    'except'    => ['yii\web\HttpException:401', 'yii\web\HttpException:403', 'yii\web\HttpException:404', 'yii\web\HttpException:406', 'yii\web\User::loginByCookie'],
                    'logVars'   => [],
                    'logTable'  => 'error_log',
                    'db'        => 'logDb'
                ],
            ],
        ],
    ],
];
