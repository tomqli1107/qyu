<?php
return [
    'components' => [
        'db' => [
            'class'     => 'yii\db\Connection',
            'dsn'       => 'mysql:host=db.abctracker.net;dbname=qyu',
            'username'  => 'tom',
            'password'  => 'xxMrMr6BzzzZmDmp',
            'charset'   => 'utf8',
        ],
        'logDb' => [
            'class'     => 'yii\db\Connection',
            'dsn'       => 'mysql:host=db.abctracker.net;dbname=qyu_log',
            'username'  => 'tom',
            'password'  => 'xxMrMr6BzzzZmDmp',
            'charset'   => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
