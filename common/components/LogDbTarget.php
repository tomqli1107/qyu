<?php

namespace common\components;

use Yii;
use yii\log\DbTarget;
use yii\helpers\VarDumper;

class LogDbTarget extends DbTarget {

    public function export() {
        // 爬虫错误不入库
        if ($_SERVER['HTTP_USER_AGENT']) {
            $spiders = explode(',', 'Spider,Googlebot,ScanBot');
            foreach($spiders as $vSpider) {
                if (stripos($_SERVER['HTTP_USER_AGENT'], $vSpider) !== false)
                    return;
            }
        }

        $context = [];
        foreach (['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'] as $name) {
            if (!empty($GLOBALS[$name])) {
                $context[] = "\${$name} = " . VarDumper::dumpAsString($GLOBALS[$name]);
            }
        }
        $params     = implode("\n\n", $context);
        $req        = php_sapi_name() != 'cli' ? Yii::$app->request : null;
        $tableName  = $this->db->quoteTableName($this->logTable);
        $sql        = "INSERT INTO {$tableName} ([[level]], [[category]], [[ip]], [[url]], [[referrer]], [[params]], [[user_agent]], [[message]], [[createtime]])
                VALUES (:level, :category, :ip, :url, :referrer, :params, :user_agent, :message, :createtime)";
        $command    = $this->db->createCommand($sql);
        foreach ($this->messages as $message) {
            list($text, $level, $category, $timestamp) = $message;
            if (!is_string($text)) {
                if ($text instanceof \Exception) {
                    $text = (string)$text;
                } else {
                    $text = VarDumper::export($text);
                }
            }
            $command->bindValues([
                ':level'        => $level,
                ':category'     => $category,
                ':ip'           => $req ? ip2long($req->getUserIP())    : '0',
                ':url'          => $req ? $req->getAbsoluteUrl()        : '',
                ':referrer'     => $req ? ($req->getReferrer() ? : '' ) : '',
                ':params'       => $params,
                ':user_agent'   => $req ? $req->getUserAgent()          : '',
                ':message'      => $text,
                ':createtime'   => $timestamp,
            ])->execute();
        }
    }
}