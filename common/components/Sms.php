<?php

namespace common\components;

use Yii;
use yii\base\Component;

class Sms extends Component {

    public  $driver;
    public  $codeLen    = 4;
    public  $duration   = 300;
    private $_client;

    public static $codeTypeMap = [
        C::SMS_CODE_TYPE_REG => '注册验证码',
    ];

    public function init() {
        $this->_client = Yii::$app->{$this->driver};
    }

    /* 发布短信到终端 */
    public function publish($type, $params) {
        switch ($type) {
            case C::SMS_TYPE_CODE:
                $phoneNum = $params[1];
                if ($phoneNum) {
                    /* 生成4位验证码 */
                    $code       = $this->_genCode($phoneNum);
                    $ele        = [C::TASKQ_TYPE_SMSCODE, [$phoneNum, $code]];
//                    $codeDesc   = self::$codeTypeMap[$params[0]] ?: '-';
//                    Yii::info("SMS Publish [{$codeDesc} {$phoneNum} {$code}]");
                    /* 写入队列 */
                    return Yii::$app->taskQueue->enqueue($ele);
                }
                break;
        }

        return false;
    }

    /* 发送验证码 */
    public function sendCode($phoneNum = '', $code = '') {
        if ($phoneNum && $code) {
            $this->_client->sendSmsCode($phoneNum, $code);
        }
    }

    /* 校验验证码 */
    public function verifyCode($phoneNum, $code) {
        return $code == Yii::$app->cache->get($this->_genKey($phoneNum));
    }

    private function _genCode($phoneNum) {
        /* 查看缓存里是否已经存在已发送的验证码 */
        $key    = $this->_genKey($phoneNum);
        $code   = Yii::$app->cache->get($key);
        if (!$code) {
            $min    = pow(10, $this->codeLen-1);
            $max    = pow(10, $this->codeLen)-1;
            $code   = mt_rand($min, $max);
            Yii::$app->cache->set($key, $code, $this->duration);
        }

        return $code;
    }

    private function _genKey($phoneNum) {
        return str_replace('[x]', $phoneNum, C::SMS_CODE_KEY);
    }
}