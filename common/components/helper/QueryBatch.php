<?php

namespace common\components\helper;

use yii\base\Component;
use yii\db\Query;

class QueryBatch extends Component {

    const PER_NUMS = 10;

    public function query(Query $query, &$data = [], $field = 'id') {
        $batches    = array_chunk($data, 10);
        $iterator   = function () use($query, $batches, $field) {
            static $i = 0;
            if ($batch = $batches[$i++])
                return $query->andWhere(['in', $field, $batch])->all();
            else
                return false;
        };

        return $iterator;
    }
}