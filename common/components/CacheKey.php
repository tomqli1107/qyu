<?php

namespace common\components;

use yii\base\Component;

class CacheKey extends Component {

    const USER_TALK = 'talk';
    const TYPE_TALK = 'talk';

    /**
     * 用户基本信息
     * user:[id]
     * ----talk:int     // 发言次数
     */
    static public function user($userId = 0) {
        return "user:{$userId}";
    }

    /**
     * 用户发言信息
     * user:talk:[type]:[id]
     * ----[typeId]:int
     */
    static public function userTalkType($typeId = 0, $userId = 0) {
        return "user:talk:{$typeId}:{$userId}";
    }

    /**
     * 频道基本信息
     * type:[type]:[typeId]
     * ----talk:int     // 发言次数
     */
    static public function type($type = 0, $typeId = 0) {
        return "type:{$type}:{$typeId}";
    }
}