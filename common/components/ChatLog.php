<?php

namespace common\components;

use common\components\helper\QueryBatch;
use Yii;
use yii\base\Component;
use yii\db\Connection;
use yii\db\Query;
use yii\di\Instance;
use common\models\Channel;
use common\models\User;
use common\models\Visitor;

class ChatLog extends Component {

    const STATUS_NORMAL = 1;
    const STATUS_CANCEL = 0;
    const TABLE_PREFIX  = 'chat_log_';
    const KEY_MEM_PREFIX= 'members:';

    public $db;

    public function init() {
        $this->db = Instance::ensure($this->db, Connection::className());
    }

    public function log($data) {
        /* @var \yii\db\Connection $this->db */
        /* @var \yii\db\Command $cmd */
        $cmd = $this->db->createCommand();
        $data['create_time'] = time();

        switch ($data['type']) {
            case C::TYPE_CHANNEL:
                $logId = intval(Channel::find()->where(['id' => $data['type_id']])->select('log_id')->scalar());
                break;
            default:
                $logId = 0;
        }

        if ($ok = $cmd->insert(self::TABLE_PREFIX.$logId, $data)->execute()) {
            /* @var \Redis $cache */
            $cache = Yii::$app->cache->getRedis();
            /* 记录聊天成员 */
            try {
                $key = $this->_genMemKey($data);
                if ($cache->zAdd($key, $this->_getScore(), $data['user_id'])) {
                    if ($cache->zCard($key) >= C::CHAT_MEMBER_LIST_MAX) {
                        $cache->zRemRangeByRank($key, 0, 0);    // 移除最早的成员
                    }
                }
            } catch (\Exception $ex) {
                Yii::error('Log Chat Member Error: ' . $ex->getMessage());
            }
            /* 统计 */
            try {
                $cache->hIncrBy(CacheKey::user($data['user_id']), CacheKey::USER_TALK, 1);
                $cache->hIncrBy(CacheKey::userTalkType($data['type'], $data['user_id']), $data['type_id'], 1);
                $cache->hIncrBy(CacheKey::type($data['type'], $data['type_id']), CacheKey::TYPE_TALK, 1);
            } catch (\Exception $ex) {
                Yii::error('Log Chat Statistics Error: ' . $ex->getMessage());
            }
        }

        return $ok;
    }

    public function history($type, $typeId, $limitVal = 0) {
        /* @var \yii\db\Command $cmd */
        $list   = [];
        $query  = new Query();
        switch ($type) {
            case C::TYPE_CHANNEL:
                $logId = intval(Channel::find()->where(['id' => $typeId])->select('log_id')->scalar());
                break;
            default:
                $logId = 0;
        }
        $query->from(self::TABLE_PREFIX.$logId)->where(['type' => $type, 'type_id' => $typeId, 'status' => self::STATUS_NORMAL])->limit(C::CHAT_HISTORY_MAX)->orderBy(['id' => SORT_DESC]);
        if ($limitVal) {
            $query->andWhere(['<', 'id', $limitVal]);
        }
        if ($logs = $query->all($this->db)) {
            $limitVal   = INF;
            $userIds    = [];
            $userInfos  = [];
            foreach ($logs as $vLog) {
                if (!$vLog['visitor'])
                    $userIds[$vLog['user_id']] = 1;
                if ($vLog['id'] < $limitVal) $limitVal = $vLog['id'];
            }
            if ($userIds) {
                $userInfos = User::find()->where(['id' => array_keys($userIds)])->select(['id', 'username', 'avatar'])->indexBy('id')->all();
            }
            foreach ($logs as $vLog) {
                $userInfo   = $userInfos[$vLog['user_id']] ?: null;
                $visitor    = !!$vLog['visitor'];
                if ($visitor) {
                    $userName   = "游客{$vLog['user_id']}";
                    $avatar     = Avatar::thumb();
                } else {
                    $userName   = $userInfo['username'] ?: '-';
                    $avatar     = Avatar::thumb($userInfo['avatar']);
                }
                /* 设置时间, 如果是今天的聊天记录, 省略日期部分, 如果是今年的, 省略年份 */
                if (date('Ymd') == date('Ymd', $vLog['create_time'])) {
                    $timeDesc = date('H:i', $vLog['create_time']);
                } elseif (date('Y') == date('Y', $vLog['create_time'])) {
                    $timeDesc = date('m-d H:i', $vLog['create_time']);
                } else {
                    $timeDesc = date('Y-m-d H:i', $vLog['create_time']);
                }

                $list[]     = [
                    'userId'    => $vLog['user_id'],
                    'username'  => $userName,
                    'avatar'    => $avatar,
                    'content'   => $vLog['content'],
                    'time'      => $vLog['create_time'],
                    'timeDesc'  => $timeDesc,
                ];
            }
        } else {
            $limitVal = 0;
        }

        return [array_reverse($list), $limitVal];
    }

    public function members($type, $typeId) {
        /* @var \Redis $cache */
        $cache  = Yii::$app->cache->getRedis();
        $key    = $this->_genMemKey(['type' => $type, 'type_id' => $typeId]);
        $members= $cache->zRange($key, 0, -1);
        $memList= [];
        $userInfoMap = [];
        if ($members) {
            $visitors   = [];
            $users      = [];
            foreach ($members as $vUserId) {
                if (Visitor::is(intval($vUserId))) {
                    $visitors[$vUserId] = 1;
                } else {
                    $users[$vUserId] = 1;
                }
            }
            if ($users) {
                $userIds    = array_keys($users);
                $queryBatch = new QueryBatch;
                $query      = User::find()->select(['id', 'username', 'sex', 'avatar', 'phone'])->asArray()->indexBy('id');
                $iter       = $queryBatch->query($query, $userIds);
                while ($vMap = $iter()) {
                    $userInfoMap += $vMap;
                }
            }
            foreach ($members as $vUserId) {
                if (Visitor::is(intval($vUserId))) {
                    $memList[] = [
                        'id'        => $vUserId,
                        'username'  => Visitor::shortName($vUserId),
                        'sex'       => 0,
                        'avatar'    => Avatar::thumb(),
                        'phone'     => '',
                    ];
                } elseif ($userInfo = $userInfoMap[$vUserId]) {
                    $userInfo['avatar'] = Avatar::thumb($userInfo['avatar']);
                    $memList[] = $userInfo;
                }
            }
        }

        return $memList;
    }

    private function _genMemKey($data) {
        return $key = self::KEY_MEM_PREFIX . "{$data['type']}:{$data['type_id']}";
    }

    private function _getScore() {
        $time   = explode(' ', microtime());
        $p1     = $time[1];
        $p2     = str_pad(ceil($time[0] * 1000), 4, '0');

        return intval("{$p1}{$p2}");
    }

}