<?php

namespace common\components;

use Yii;

class Response {

    protected $type;

    public function __construct($type = 'json') {
        $this->type = $type;
    }

    public function succ($data = null) {
        Yii::$app->response->format = $this->type;

        return [
            'code'      => C::CODE_SUCC,
            'login'     => Yii::$app->getUser()->getIsGuest() == false,
            'data'      => $data ?: [],
        ];
    }

    public function fail($code = C::CODE_ERROR, $msg = '') {
        Yii::$app->response->format = $this->type;

        if (is_array($msg) || is_object($msg))
            $msg = json_encode($msg);

        return [
            'code'      => $code,
            'message'   => $msg ? : '',
            'login'     => Yii::$app->getUser()->getIsGuest() == false,
        ];
    }


}