<?php

namespace common\components;

class Validator extends \yii\validators\Validator {

    public $scene;
    public $skipOnEmpty = false;
}