<?php

namespace common\components;

use Yii;
use yii\base\Component;

class TaskQueue extends Component {

    public  $driver;
    private $_client;

    public function init() {
        $this->_client = Yii::$app->{$this->driver};
    }

    public function enqueue($ele = null) {
        if ($ele) {
            $type   = intval($ele[0]);
            $params = [];
            if (is_array($ele[1])) {
                foreach($ele[1] as $v) {
                    if (is_string($v)) {
                        $params[] = $v;
                    } elseif(is_scalar($v)) {
                        $params[] = strval($v);
                    } else {
                        continue;
                    }
                }
            }
            if ($type && $params) {
                $newEle = [
                    'type'      => $type,
                    'params'    => $params,
                ];
                return $this->_client->lPush(C::TASKQ_KEY, json_encode($newEle));
            }
        }

        return false;
    }

}