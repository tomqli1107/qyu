<?php

namespace common\components\ali;

require_once(\Yii::$app->vendorPath . '/ali/aliyun-oss-php-sdk/autoload.php');

use Yii;
use yii\base\Component;
use OSS\OssClient;
use OSS\Core\OssException;

class AliyunOSS extends Component {

    public $accessId;
    public $accessKey;
    public $endpoint;
    public $bucket;
    public $avatarDir;
    public $suffix;
    public $_client;

    public function init() {
        try {
            $this->_client = new OssClient($this->accessId, $this->accessKey, $this->endpoint);
        } catch (OssException $e) {
            Yii::error('Creating OssClient Instance FAIL:' . $e->getMessage());
        }
    }

    public function uploadAvatar($name, $content) {
        $ymd    = date('Ymd');
        $key    = $this->_genKey($name);
        $object = "{$this->avatarDir}/{$ymd}/{$key}{$this->suffix}";
        $content= base64_decode(substr(strstr($content, ','), 1));
        try{
            return $this->_client->putObject($this->bucket, $object, $content);
        } catch(OssException $e) {
            Yii::error('OssClient Upload Avatar FAIL:' . $e->getMessage());
        }

        return false;
    }

    public function deleteAvatar($url) {
        try{
            return $this->_client->deleteObject($this->bucket, strstr($url, $this->avatarDir));
        } catch(OssException $e) {
            Yii::error('OssClient Delete Avatar FAIL:' . $e->getMessage());
        }

        return false;
    }

    private function _genKey($name) {
        $suffix = microtime(true)*1000;
        $rand   = mt_rand(100, 9999);
        $key    = md5($name.$suffix.$rand);
        $i      = mt_rand(0, 30);

        return substr($key, $i, 2) . '-' . $key;
    }
}