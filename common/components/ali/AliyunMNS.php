<?php

namespace common\components\ali;

require_once(\Yii::$app->vendorPath . '/ali/mns-autoloader.php');

use yii\base\Component;
use AliyunMNS\Client;
use AliyunMNS\Model\BatchSmsAttributes;
use AliyunMNS\Model\MessageAttributes;
use AliyunMNS\Exception\MnsException;
use AliyunMNS\Requests\PublishMessageRequest;

class AliyunMNS extends Component {

    public $endpoint;
    public $accessId;
    public $accessKey;
    public $smsTopicName;
    public $smsSignName;
    public $smsTemplateCode;
    public $client;

    public function init() {
        /**
         * Step 1. 初始化Client
         */
        $this->client = new Client($this->endpoint, $this->accessId, $this->accessKey);
    }

    public function sendSmsCode($phoneNum, $code) {
        /**
         * Step 2. 获取主题引用
         */
        $topic = $this->client->getTopicRef($this->smsTopicName);
        /**
         * Step 3. 生成SMS消息属性
         */
        // 3.1 设置发送短信的签名（SMSSignName）和模板（SMSTemplateCode）
        $batchSmsAttributes = new BatchSmsAttributes($this->smsSignName, $this->smsTemplateCode);
        // 3.2 （如果在短信模板中定义了参数）指定短信模板中对应参数的值
        $batchSmsAttributes->addReceiver($phoneNum, array('code' => $code));
//        $batchSmsAttributes->addReceiver("YourReceiverPhoneNumber2", array("YourSMSTemplateParamKey1" => "value1"));
        $messageAttributes = new MessageAttributes(array($batchSmsAttributes));
        /**
         * Step 4. 设置SMS消息体（必须）
         *
         * 注：目前暂时不支持消息内容为空，需要指定消息内容，不为空即可。
         */
        $messageBody = "smsmessage";
        /**
         * Step 5. 发布SMS消息
         */
        $request = new PublishMessageRequest($messageBody, $messageAttributes);
        try
        {
            $res = $topic->publishMessage($request);
            echo $res->isSucceed();
            echo "\n";
            echo $res->getMessageId();
            echo "\n";
        }
        catch (MnsException $e)
        {
            echo $e;
            echo "\n";
        }
    }
}