<?php

namespace common\components;

class Controller extends \yii\web\Controller {
    protected $r;

    public function init() {
        parent::init();

        /* 关闭NOTICE */
        error_reporting(E_ALL ^ E_NOTICE);
        /* 设置响应组件 */
        $this->r = new Response;
        /* 关闭CSRF */
        $this->enableCsrfValidation = false;
        /* 设置时区 */
        date_default_timezone_set('Asia/Shanghai');
    }

    public function succ($data = null) {
        return $this->r->succ($data);
    }
    public function fail($code = C::CODE_ERROR, $msg = '') {
        return $this->r->fail($code, $msg);
    }
}