<?php

namespace common\components;

use yii\caching\Cache;


class RedisCache extends Cache {

    public $host        = 'localhost';
    public $port        = 6379;
    public $database    = 0;
    public $password;

    /**
     * 是否启用长连接
     * @var bool
     */
    public $persistent = false;

    /**
     * @var \Redis $_redis
     */
    private $_redis = null;

    public function init() {
        parent::init();
        $this->getRedis();
    }

    /**
     * @return \Redis
     */
    public function getRedis() {
        if (!$this->_redis) {
            $this->_redis = new \Redis();
            if ($this->persistent) {
                $this->_redis->pconnect($this->host, $this->port);
            } else {
                $this->_redis->connect($this->host, $this->port);
            }
            if ($this->password) {
                $this->_redis->auth($this->password);
            }
            if ($this->database) {
                $this->_redis->select($this->database);
            }
        }

        return $this->_redis;
    }

    public function lPush($key, $val) {
        return $this->_redis->lPush($key, $val);
    }

    /**
     * Retrieves a value from cache with a specified key.
     * This is the implementation of the method declared in the parent class.
     * @param string $key a unique key identifying the cached value
     * @return string|boolean the value stored in cache, false if the value is not in the cache or expired.
     */
    protected function getValue($key) {
        return $this->_redis->get($key);
    }

    /**
     * Stores a value identified by a key in cache.
     * This is the implementation of the method declared in the parent class.
     *
     * @param string $key the key identifying the value to be cached
     * @param string $value the value to be cached
     * @param integer $duration the number of seconds in which the cached value will expire. 0 means never expire.
     * @return boolean true if the value is successfully stored into cache, false otherwise
     */
    protected function setValue($key, $value, $duration) {
        if ($duration==0) {
            return $this->_redis->set($key, $value);
        }

        return $this->_redis->setex($key, $duration, $value);
    }

    /**
     * Stores a value identified by a key into cache if the cache does not contain this key.
     * This is the implementation of the method declared in the parent class.
     *
     * @param string $key the key identifying the value to be cached
     * @param string $value the value to be cached
     * @param integer $duration the number of seconds in which the cached value will expire. 0 means never expire.
     * @return boolean true if the value is successfully stored into cache, false otherwise
     */
    protected function addValue($key, $value, $duration) {
        return $this->setValue($key, $value, $duration);
    }

    /**
     * Deletes a value with the specified key from cache
     * This is the implementation of the method declared in the parent class.
     * @param string $key the key of the value to be deleted
     * @return boolean if no error happens during deletion
     */
    protected function deleteValue($key) {
        return $this->_redis->del($key);
    }

    /**
     * Deletes all values from cache.
     * This is the implementation of the method declared in the parent class.
     * @return boolean whether the flush operation was successful.
     */
    protected function flushValues() {
        return $this->_redis->flushDB();
    }

    public function __destruct() {
        $this->_redis && $this->_redis->close();
    }
}