<?php

namespace common\components;

class Avatar {

    public static function thumb($url = null, $style = Image::STYLE_XSMALLSQUARE) {
        $url = $url ?: C::DEFAULT_AVATAR;

        return Image::combine($url, $style);
    }
}