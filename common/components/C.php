<?php

namespace common\components;

class C {

    /* 默认头像 */
    const DEFAULT_AVATAR = 'http://qyu-public.oss-cn-shenzhen.aliyuncs.com/avatar/5ff896c5c9e88ad40dbef38b752cba33.png';

    /* 自动登录周期, 一周 */
    const SESS_DURATION = 604800;


    /* 游客cookies */
    const FT_COK_VISITOR_N = '_visitor_';

    /* 返回CODE */
    const CODE_SUCC     = 0;
    const CODE_ERROR    = 10000;
    const CODE_FAIL     = 10001;
    const CODE_VALID    = 10002;
    const CODE_AUTH     = 10009;

    /* 全局类型 */
    const TYPE_CHANNEL  = 1;

    /* SMS */
    /****************************************/
    /* 类型 */
    const SMS_TYPE_CODE = 1;
    /* CODE类型 */
    const SMS_CODE_TYPE_REG  = 1;
    /* CODE的缓存KEY, [x]=手机号码 */
    const SMS_CODE_KEY = 'qyu:sms:code:[x]';
    /****************************************/

    /* 任务队列 */
    /****************************************/
    /* 类型 */
    const TASKQ_TYPE_SMSCODE = 1;
    /* KEY */
    const TASKQ_KEY = 'qyu:task:queue';
    /****************************************/

    /* 场景 */
    const SCENE_REG     = 1;
    const SCENE_LOGIN   = 2;

    /* 登录类型 */
    const LOGIN_TYPE_PW     = 'pw';
    const LOGIN_TYPE_CODE   = 'code';
    const LOGIN_TYPE_WX     = 'wx';

    /* 聊天 */
    const CHAT_HISTORY_MAX      = 10;
    const CHAT_MEMBER_LIST_MAX  = 50;

}