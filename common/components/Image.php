<?php

namespace common\components;

class Image {

    const IDF                   = '!';
    const STYLE_SMALLSQUARE     = 'smallsquare';
    const STYLE_XSMALLSQUARE    = 'xsmallsquare';
    const STYLE_XXSMALLSQUARE   = 'xxsmallsquare';
    const STYLE_LARGESQUARE     = 'largesquare';
    const STYLE_MEDIUMSQURE     = 'mediumsqure';

    public static function combine($url, $style = self::STYLE_XSMALLSQUARE) {
        return $url . self::IDF . $style;
    }

}