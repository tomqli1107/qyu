<?php
namespace common\models;

use common\components\C;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE     = 1;
    const STATUS_PAUSED     = 2;

    const SCENE_REG_PHONE   = 'reg_phone';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        return [
//            TimestampBehavior::className(),
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'password'], 'required', 'on' => self::SCENE_REG_PHONE],
            ['username', 'required'],
            [['sex', 'update_time', 'create_time'], 'default', 'value' => 0],
            ['sex', 'in', 'range' => [0, 1, 2]],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_PAUSED]],
            ['avatar', 'string', 'max' => 255],
            ['auth_key', 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function existByPhone($phoneNum = '') {
        return User::find()->where(['phone' => $phoneNum])->limit(1)->select('id')->scalar() ? true : false;
    }

    /**
     * 通过手机号注册创建新用户
     */
    public static function newByPhone($phoneNum, $password) {
        $nowTime            = time();
        $user               = new User;
        $user->scenario     = User::SCENE_REG_PHONE;
        $user->attributes   = [
            'phone'         => $phoneNum,
            'username'      => str_replace(substr($phoneNum, 4, 4), '****', $phoneNum),
            'create_time'   => $nowTime,
            'update_time'   => $nowTime,
        ];
        $user->setPassword($password);
        $user->generateAuthKey();
        if ($user->validate()) {
            if ($user->save()) {
                return [true, $user];
            }
        } else {
            return [false, C::CODE_VALID];
        }

        return [false, C::CODE_FAIL];
    }

    /**
     * 通过手机号查找用户
     */
    public static function findByPhone($phoneNum) {
        return static::findOne(['phone' => $phoneNum]);
    }

    /**
     * 判断用户状态是否挂起
     */
    public function isHangUp() {
        return $this->status != self::STATUS_ACTIVE;
    }

    /**
     * 判断用户能否登录
     */
    public function allowLogin() {
        return $this->status == self::STATUS_ACTIVE;
    }

    /**
     * 用户登录操作
     */
    public function login() {
        Yii::$app->user->login($this, C::SESS_DURATION);
    }

    /**
     * 获取一个游客id
     */
    public static function getVisitorId() {
        return Visitor::id();
    }

    /**
     * 编辑用户资料
     */
    public static function edit($data) {
        return User::updateAll([
            'username'      => $data['username'],
            'sex'           => intval($data['sex']),
            'birthday'      => intval($data['birthday']),
            'update_time'   => time(),
        ], ['id' => $data['id']]);
    }
}
