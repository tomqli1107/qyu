<?php
/**
 * Created by PhpStorm.
 * User: tom
 * Date: 2017/5/15
 * Time: 16:22
 */

namespace common\models;

use yii\db\ActiveRecord;

class Server extends ActiveRecord {

    const TYPE_WS       = 1;
    const TYPE_CACHE    = 2;

    /**
     * 分配一个服务器
     */
    public static function dispatch() {
        if ($serverIds = self::find()->where(['type' => self::TYPE_WS])->select('id')->column()) {
            shuffle($serverIds);

            return array_shift($serverIds);
        }

        return false;
    }

}