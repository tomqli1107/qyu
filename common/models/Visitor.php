<?php

namespace common\models;

use Yii;
use common\components\C;
use yii\web\Cookie;
use yii\db\ActiveRecord;


class Visitor extends ActiveRecord {

    const ID_LIMIT = 10000000000;

    public static function generate() {
        /* 随机生成一个游客id */
        $visitorId = intval(microtime(true)*10000) . mt_rand(100, 999);
        /* 设置cookies */
        Yii::$app->response->cookies->add(new Cookie([
            'name'      => C::FT_COK_VISITOR_N,
            'value'     => $visitorId,
            'expire'    => strtotime('+1 week')
        ]));
        /* 入库 */
        try {
            $mod                = new Visitor;
            $mod->visitor_id    = $visitorId;
            $mod->agent         = Yii::$app->request->userAgent;
            $mod->create_time   = time();
            $mod->save();
        } catch (\Exception $ex) {
            Yii::error("游客[{$visitorId}]入库失败:" . $ex->getMessage());
        }

        return $visitorId;
    }

    public static function id() {
        $visitorId = intval(Yii::$app->request->cookies->getValue(C::FT_COK_VISITOR_N, 0));
        if (!$visitorId) {
            $visitorId = self::generate();
        }

        return $visitorId;
    }

    public static function detail() {
        $id = self::id();
        return [
            'id'        => $id,
            'name'      => self::name($id),
            'createTime'=> date('Y-m-d', Visitor::find()->where(['visitor_id' => $id])->select('create_time')->scalar()?:time()),
        ];
    }

    public static function name($visitorId = 0) {
        return "游客{$visitorId}";
    }

    public static function shortName($visitorId = 0) {
        return '*'.substr($visitorId, -7);
    }

    public static function is($userId = 0) {
        return $userId > self::ID_LIMIT;
    }
}