<?php

namespace common\models;

use yii\db\ActiveRecord;


class Channel extends ActiveRecord {

    const TYPE_INTERNAL = 1;
    const TYPE_SEARCH   = 2;

    public static function search($input = '') {
        if (!$input) return false;

        $key        = self::genKey($input);
        $channelId  = self::find()->where(['key' => $key, 'type' => self::TYPE_SEARCH])->select('id')->limit(1)->scalar();
        if (!$channelId) {
            /* 创建一个频道 */
            $channelId = self::create([
                'key'   => $key,
                'input' => $input,
                'type'  => self::TYPE_SEARCH,
            ]);
        }

        return $channelId ?: false;
    }

    public static function create($data = []) {
        if (!$data) return false;

        /* 分配一个服务器 */
        if ($serverId = Server::dispatch()) {
            $channel                = new Channel;
            $channel->key           = $data['key'];
            $channel->name          = $data['input'];
            $channel->type          = $data['type'];
            $channel->server_id     = $serverId;
            $channel->create_time   = time();
            if ($channel->save()) {
                return $channel->id;
            }
        }

        return false;
    }

    public static function genKey($input = '') {
        if (!$input) return false;

        $key1 = md5($input);
        $key2 = md5(strlen($input));

        return "{$key1}{$key2}";
    }
}