define(['app', 'vue', 'CONST', 'EVENT', 'CONFIG'], function(app, Vue, CONST, EVENT, CONFIG) {

    function ChatMembers() {
        var $this, $main;
        var _page;

        this.export = function() {
            $this = this;
            return {
                init: function() {
                    $this.init();
                }
            };
        };
        this.init = function() {
            _page = $(app.$f7.getCurrentView().activePage.container);
            app.$mains.push(this.initVue().init());
        };
        this.initVue = function() {
            $main = new Vue({
                el: _page.find('.vue-app').get(0),
                data: function() {
                    return {

                    };
                },
                mounted: function() {},
                methods: {
                    init: function() {
                        /* 显示页面 */
                        $(this.$el).show();

                        return $main;
                    }
                }
            });

            return $main;
        };
    }

    return (new ChatMembers()).export();
});