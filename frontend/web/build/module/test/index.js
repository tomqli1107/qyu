define(['app', 'vue', 'vuex', 'CONST'], function(app, Vue, Vuex, CONST) {

    function TestIndex() {
        var $this, $export;
        var $f7, $mainView, $vue, $store;
        var _pageContent, _talkBar, _chatBox, _navBar, _chatBoard;
        var orgWH, diffWH;
        var talkBarViewSeed;

        /* 频道id */
        var channelId = 0;

        this.export = function() {
            $f7         = app.$f7;
            $mainView   = app.$mainView;
            $this       = this;
            $export     = {
                init: function(query) {
                    $this.init(query)
                }
            };

            return $export;
        };
        this.init = function(query) {
            if (query && query.id) {
                channelId = query.id;
                this.initVue().init();
            }
        };
        this.initVue = function() {
            $store = new Vuex.Store({
                state: {
                    channelId   : 0,
                    chatList    : [],
                    tmpChatList : [],
                    editing     : false
                },
                mutations: {
                    talk: function() {
                        $vue.talkInput = '';
                    },
                    receive: function(state, msgEle) {
                        if (state.channelId != msgEle.channelId) return;

                        $vue.pushChatList(state.chatList, msgEle);
                        /*  */
                        var boxH    = _chatBox.height();
                        var boardH  = _chatBoard.height();
                        var diffH   = boardH - boxH;
                        if (diffH < 0) diffH = 0;
                        diffH -= 100;
                        if (Math.abs(_chatBoard.position().top) > diffH) {
                            $vue.scrollToChatBottom();
                        }
                    },
                    initChat: function(state, params) {
                        state.channelId = params.channelId;
                        state.chatList  = params.chatLog || [];
                    },
                    fetchHistory: function(state, logs) {
                        ++$vue.historyLoop;
                        state.tmpChatList = [];
                        var isInit = $vue.historyLoop === 1;
                        logs.forEach(function(msgEle) {
                            if (!isInit) msgEle.loop = $vue.historyLoop;
                            $vue.pushChatList(isInit?state.chatList:state.tmpChatList, msgEle);
                        });
                        if (isInit) {
                            $vue.scrollToChatBottom();
                        } else {
                            state.chatList = state.tmpChatList.concat(state.chatList);
                        }
                    }
                },
                actions: {
                    talk: function(ctx) {
                        if (!$vue.talkInput) return;
                        /* 执行说话 */
                        app.$user.talk($vue.talkInput);
                        ctx.commit(CONST.ACT_TALK);
                    },
                    receive: function(ctx, msgEle) {
                        ctx.commit(CONST.ACT_RECEIVE, msgEle);
                    },
                    initChat: function(ctx) {
                        $store.dispatch(CONST.ACT_FETCH_HISTORY);
                        ctx.commit(CONST.ACT_INIT_CHAT, {channelId: channelId});
                    },
                    fetchHistory: function(ctx) {
                        $vue.fetchHistory($vue.limitVal).done(function(logs) {
                            ctx.commit(CONST.ACT_FETCH_HISTORY, logs);
                        });
                    }
                }
            });
            $vue = new Vue({
                el: $(app.$f7.getCurrentView().activePage.container).find('.vue-app').get(0),
                store: $store,
                computed: {
                    chatList: function() {
                        return this.$store.state.chatList;
                    }
                },
                data: function() {
                    return {
                        historyLoop : 0,
                        limitVal    : 0,
                        refresh     : false,
                        refreshDone : false,
                        talkInput   : '',
                        debug       : ''
                    };
                },
                mounted: function() {
                    _pageContent= $(this.$refs.pageContent);
                    _talkBar    = $(this.$refs.talkBar);
                    _chatBox    = $(this.$refs.chatBox);
                    _navBar     = $(this.$refs.navBar);
                    _chatBoard  = _chatBox.find('.chat-board');
                    orgWH       = app._window.outerHeight();
                },
                methods: {
                    init: function() {
                        /* 修正chatBox的高度 */
                        setTimeout(this.resizeChatBoxHeight, 100);
                        /* 用户进入频道 */
                        app.$user.channel(channelId).done(function() {
                            /* 修正频道标题 */
                            _navBar.find('.channel-name').text(app.$channels[channelId].detail.name);
                            /* 绑定接收器 */
                            app.$wsAcceptor.callback(CONST.WS_ELE_TYPE_TALK, function(msgEle) {
                                $vue.$store.dispatch(CONST.ACT_RECEIVE, msgEle);
                            });
                        }).fail(function() {
                            console.log('进入频道失败');
                        });
                        /* 初始化聊天 */
                        this.$store.dispatch(CONST.ACT_INIT_CHAT, channelId);
                        /* 显示页面 */
                        $(this.$el).show();
                        /* 监控chatBoard下拉 */
                        $$(_chatBoard.get(0)).touchmove(function(e) {
                            if (_chatBoard.position().top > 80) {
                                if ($vue.refresh || $vue.refreshDone) return;
                                $vue.refresh    = true;
                                $vue.refreshDone= true;
                                $vue.$store.dispatch(CONST.ACT_FETCH_HISTORY);
                            }
                            if (_chatBoard.position().top < 10) {
                                $vue.refreshDone = false;
                            }
                        });
                    },
                    fetchHistory: function(limitVal) {
                        var $def = $.Deferred();
                        app.GET('/chat/history', {type: CONST.TYPE_CHANNEL, type_id: channelId, limit_val: limitVal}).done(function(data) {
                            if (data.logs && data.logs.length) {
                                $vue.limitVal = data.limitVal;
                                $def.resolve(data.logs);
                            }
                        }).always(function() {
                            $vue.refresh = false;
                        });

                        return $def;
                    },
                    pushChatList: function(chatList, msgEle) {
                        /* 判断是否需要插入时间 */
                        var last;
                        if (chatList.length) {
                            last = chatList[chatList.length- 1];
                            if (last.type != CONST.TALK_ELE_TYPE_TIME) {
                                /* 如果与上一条聊天时间差超过1分钟, 插入时间, 且必须显示昵称 */
                                if (parseInt(msgEle.time) - parseInt(last.time) > 60) {
                                    last = app.makeTalkEle({
                                        type    : CONST.TALK_ELE_TYPE_TIME,
                                        time    : msgEle.time,
                                        timeDesc: msgEle.timeDesc,
                                        loop    : msgEle.loop
                                    });
                                    chatList.push(last);
                                }
                            }
                        } else {
                            last = app.makeTalkEle({
                                type    : CONST.TALK_ELE_TYPE_TIME,
                                time    : msgEle.time,
                                timeDesc: msgEle.timeDesc,
                                loop    : msgEle.loop
                            });
                            chatList.push(last);
                        }

                        var params = {
                            userId  : msgEle.userId,
                            username: msgEle.username,
                            avatar  : msgEle.avatar,
                            content : msgEle.content,
                            time    : msgEle.time,
                            isSame  : true
                        };

                        /* 如果上一条聊天是同一个用户, 省略昵称显示 */
                        if (!chatList.length || last.type == CONST.TALK_ELE_TYPE_TIME || last.user.id!=msgEle.userId) {
                            params.isSame = false;
                        }
                        chatList.push(app.makeTalkEle(params));
                    },
                    scrollToChatBottom: function() {
                        Vue.nextTick(function() {
                            _chatBox.find('.talk-ele:last').velocity('scroll', {duration: 1000, container: _chatBox});
                        });
                    },
                    resizeChatBoxHeight: function() {
                        var pH  = _pageContent.outerHeight();
                        var nH  = _navBar.outerHeight();
                        var tH  = _talkBar.outerHeight();
                        var h   = pH - nH - tH;
                        _chatBox.outerHeight(h);
                        //_chatBox.outerHeight(h+100);
                        _chatBoard.css('minHeight', h+1+'px');
                    },
                    talk: function() {
                        this.$store.dispatch(CONST.ACT_TALK);
                        _pageContent.find('.input-area textarea').focus();
                    },
                    talkInputFocus: function() {
                        talkBarViewSeed = setInterval(function() {
                            _talkBar.get(0).scrollIntoView();
                        }, 100);
                        setTimeout(function() {
                            if (talkBarViewSeed) {
                                clearInterval(talkBarViewSeed);
                            }
                        }, 2000);
                        setTimeout(function() {
                            diffWH = orgWH - app._window.outerHeight();
                            _chatBoard.velocity({paddingTop: diffWH+'px'}, {duration: 0});
                            $vue.scrollToChatBottom();
                        }, 100);
                        this.editing = true;
                    },
                    talkInputBlur: function() {
                        if (talkBarViewSeed) {
                            clearInterval(talkBarViewSeed);
                        }
                        this.editing = false;
                        setTimeout(function() {
                            if (!$vue.editing) {
                                _chatBoard.velocity({paddingTop: 0}, {duration: 500});
                            }
                        });
                    }
                }
            });

            return $vue;
        };
    }

    return (new TestIndex()).export();
});