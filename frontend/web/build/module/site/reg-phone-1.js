define(['app', 'vue', 'CONST', 'CONFIG'], function(app, Vue, CONST, CONFIG) {

    function SiteRegPhone1() {
        var $this, $main;
        var _page, _pageContent;

        this.export = function() {
            $this = this;
            return {
                init: function() {
                    $this.init();
                }
            };
        };
        this.init = function() {
            _page       = $(app.$f7.getCurrentView().activePage.container);
            _pageContent= _page.find('.page-content');
            app.$mains.push(this.initVue().init());
        };
        this.initVue = function() {
            $main = new Vue({
                el: _page.find('.vue-app').get(0),
                data: function() {
                    return {
                        lockGetCode     : false,
                        lockRegister    : false,
                        inputWrong      : true,
                        phoneInput      : '',
                        codeInput       : '',
                        passwordInput   : '',
                        msgNoticeText   : '',
                        codeNoticeText  : '',
                        codeInterval    : CONST.SMS_CODE_INTERVAL
                    };
                },
                watch: {
                    phoneInput: function() {
                        $main.$nextTick(function() {
                            this.phoneInput = this.phoneInput.toString().substr(0, 11);
                        });
                        this.watchInput();
                    },
                    codeInput: function() {
                        $main.$nextTick(function() {
                            this.codeInput = this.codeInput.toString().substr(0, 4);
                        });
                        this.watchInput();
                    },
                    passwordInput: function() {
                        this.watchInput();
                    }
                },
                mounted: function() {},
                methods: {
                    init: function() {
                        $(this.$el).show();

                        return $main;
                    },
                    getCode: function() {
                        if (!this.lockGetCode) {
                            $main.setMsgNotice('');
                            if (this.phoneInput) {
                                $main.lockGetCode = true;
                                app.POST(CONFIG.POST_PHONE_SEND_SMS_CODE, {type: CONST.SMS_CODE_TYPE_REG, phone_num: $main.phoneInput}).done(function() {
                                    /* 发送成功 */
                                    var func, seed;
                                    func = function() {
                                        if ($main.codeInterval == 0) {
                                            $main.codeInterval   = CONST.SMS_CODE_INTERVAL;
                                            $main.lockGetCode    = false;
                                            $main.setCodeNotice('');
                                            clearInterval(seed);
                                        } else {
                                            $main.setCodeNotice('短信已发出，'+$main.codeInterval+'秒后可再次获取');
                                        }
                                        --$main.codeInterval;
                                    };
                                    func();
                                    seed = setInterval(func, 1000);
                                }).fail(function(er) {
                                    $main.lockGetCode = false;
                                    $main.setMsgNotice(er.message);
                                });
                            } else {
                                $main.setMsgNotice('请输入手机号码');
                            }
                        }
                    },
                    watchInput: function() {
                        if (/^1[0-9]{10}$/.test(this.phoneInput) && /^\d{4}$/.test(this.codeInput) && /^\S{6,18}$/.test(this.passwordInput)) {
                            this.inputWrong = false;
                        }
                    },
                    clearInput: function(type) {
                        switch(type) {
                            case 1:
                                this.phoneInput = '';
                                break;
                            case 3:
                                this.passwordInput = '';
                        }
                    },
                    setMsgNotice: function(msg) {
                        this.msgNoticeText = msg;
                    },
                    setCodeNotice: function(msg) {
                        this.codeNoticeText = msg;
                    },
                    register: function() {
                        if (this.lockRegister || this.inputWrong) return;
                        this.lockRegister = true;
                        $main.setMsgNotice('');
                        app.POST(CONFIG.POST_REG_PHONE, {phone_num: $main.phoneInput, code: $main.codeInput, password: $main.passwordInput}).done(function(data) {
                            app.$user.identity(data);
                            /* 注册成功时进行下一步渲染 */
                            $main.finishReg();
                        }).fail(function(er) {
                            $main.setMsgNotice(er.message);
                        }).always(function() {
                            $main.lockRegister = false;
                        });
                    },
                    finishReg: function() {
                        var _reg = _pageContent.find('.reg');
                        var _fin = _pageContent.find('.fin');
                        _reg.addClass('animated').addClass('zoomOut');
                        setTimeout(function() {
                            _reg.remove();
                            _fin.removeClass('hidden').addClass('animated').addClass('zoomIn');
                        }, 1000);
                    },
                    start: function() {
                        app.loadPage({
                            url     : CONST.PAGE_HOME,
                            view    : CONST.MAIN_VIEW
                        });
                    }
                }
            });

            return $main;
        };
    }

    return (new SiteRegPhone1()).export();
});