define(['app', 'vue', 'CONST', 'CONFIG'], function(app, Vue, CONST, CONFIG) {

    function SiteLogin() {
        var $this, $main;
        var _page;

        var LOGIN_TYPE_PW   = 'pw';
        var LOGIN_TYPE_CODE = 'code';

        this.export = function() {
            $this = this;
            return {
                init: function() {
                    $this.init();
                }
            };
        };
        this.init = function() {
            _page = $(app.$f7.getCurrentView().activePage.container);
            app.$mains.push(this.initVue().init());
        };
        this.initVue = function() {
            $main = new Vue({
                el: _page.find('.vue-app').get(0),
                data: function() {
                    return {
                        show            : false,
                        loginType       : LOGIN_TYPE_PW,
                        lockGetCode     : false,
                        lockLogin       : false,
                        inputWrong      : true,
                        phoneInput      : '',
                        codeInput       : '',
                        passwordInput   : '',
                        msgNoticeText   : '',
                        codeNoticeText  : '',
                        loginNoticeText : '',
                        hadLogin        : false,
                        codeInterval    : CONST.SMS_CODE_INTERVAL
                    };
                },
                watch: {
                    phoneInput: function() {
                        $main.$nextTick(function() {
                            this.phoneInput = this.phoneInput.toString().substr(0, 11);
                        });
                        this.watchInput();
                    },
                    codeInput: function() {
                        $main.$nextTick(function() {
                            this.codeInput = this.codeInput.toString().substr(0, 4);
                        });
                        this.watchInput();
                    },
                    passwordInput: function() {
                        this.watchInput();
                    }
                },
                mounted: function() {},
                methods: {
                    init: function() {
                        setTimeout(function() {
                            $main.show = true;
                        }, 200);
                        $(this.$el).show();

                        return $main;
                    },
                    getCode: function() {
                        if (!this.lockGetCode) {
                            $main.setMsgNotice('');
                            if (this.phoneInput) {
                                $main.lockGetCode = true;
                                app.POST(CONFIG.POST_PHONE_SEND_SMS_CODE, {type: CONST.SMS_CODE_TYPE_REG, phone_num: $main.phoneInput}).done(function() {
                                    /* 发送成功 */
                                    var func, seed;
                                    func = function() {
                                        if ($main.codeInterval == 0) {
                                            $main.codeInterval   = CONST.SMS_CODE_INTERVAL;
                                            $main.lockGetCode    = false;
                                            $main.setCodeNotice('');
                                            clearInterval(seed);
                                        } else {
                                            if (!$main.hadLogin) {
                                                $main.setCodeNotice('短信已发出，'+$main.codeInterval+'秒后可再次获取');
                                            }
                                        }
                                        --$main.codeInterval;
                                    };
                                    func();
                                    seed = setInterval(func, 1000);
                                }).fail(function(er) {
                                    $main.lockGetCode = false;
                                    $main.setMsgNotice(er.message);
                                });
                            } else {
                                $main.setMsgNotice('请输入手机号码');
                            }
                        }
                    },
                    watchInput: function() {
                        if (/^1[0-9]{10}$/.test(this.phoneInput)) {
                            if (this.loginType == LOGIN_TYPE_PW && /^\S{6,18}$/.test(this.passwordInput)) {
                                this.inputWrong = false;
                            } else if (this.loginType == LOGIN_TYPE_CODE && /^\d{4}$/.test(this.codeInput)) {
                                this.inputWrong = false;
                            }
                        }
                    },
                    clearInput: function(type) {
                        switch(type) {
                            case 1:
                                this.phoneInput = '';
                                break;
                            case 3:
                                this.passwordInput = '';
                        }
                    },
                    setMsgNotice: function(msg) {
                        this.msgNoticeText = msg;
                    },
                    setCodeNotice: function(msg) {
                        this.codeNoticeText     = msg;
                        this.loginNoticeText    = '';
                    },
                    setLoginNotice: function(msg) {
                        this.loginNoticeText    = msg;
                        this.codeNoticeText     = '';
                    },
                    login: function() {
                        if (this.lockLogin || this.inputWrong || this.hadLogin) return;
                        this.lockLogin = true;
                        $main.setMsgNotice('');
                        app.POST(CONFIG.POST_LOGIN, {phone_num: $main.phoneInput, code: $main.codeInput, password: $main.passwordInput, type: $main.loginType}).done(function(data) {
                            app.$user.identity(data);
                            /* 登录成功时进行下一步渲染 */
                            $main.hadLogin = true;
                            $main.setLoginNotice('登录成功');
                            setTimeout(function() {
                                $main.start();
                            }, 1000);
                        }).fail(function(er) {
                            $main.setMsgNotice(er.message);
                        }).always(function() {
                            $main.lockLogin = false;
                        });
                    },
                    start: function() {
                        app.loadPage({
                            url     : CONST.PAGE_HOME,
                            view    : CONST.MAIN_VIEW
                        });
                    },
                    switchLoginType: function(tab) {
                        console.log(tab);
                    }
                }
            });

            return $main;
        };
    }

    return (new SiteLogin()).export();
});