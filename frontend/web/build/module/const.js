define({
    'GET'   : 'GET',
    'POST'  : 'POST',
    'ERROR'     : 1,
    'EXCEPTION' : 2,

    /* 验证码间隔 */
    'SMS_CODE_TYPE_REG': 1,
    'SMS_CODE_INTERVAL': 60,

    /* 背景类型 */
    'BG_DEFAULT' : 1,

    /* 消息单元类型 */
    'WS_ELE_TYPE_TALK'      : 1,
    'WS_ELE_TYPE_REG'       : 2,
    'WS_ELE_TYPE_CHANNEL'   : 3,
    /* Talk Element类型 */
    'TALK_ELE_TYPE_SELF'    : 1,
    'TALK_ELE_TYPE_OTHER'   : 2,
    'TALK_ELE_TYPE_TIME'    : 3,
    /* WS关闭事件code */
    'WS_CLOSE_CODE_TIMEOUT' : 1006,

    /* Vuex Actions */
    'ACT_TALK'      : 'talk',
    'ACT_RECEIVE'   : 'receive',
    'ACT_INIT_CHAT' : 'initChat',
    'ACT_INIT_HISTORY' : 'initHistory',
    'ACT_FETCH_HISTORY': 'fetchHistory',

    /* PAGE URL */
    'PAGE_HOME'         : 'page/home/index.html',
    'PAGE_CHANNEL'      : 'page/chat/index.html',
    'PAGE_REG_PHONE_1'  : 'page/site/reg-phone-1.html',

    /* POPOVER NAME */
    'POPUP_USER_AVATAR_STATION': 'popup/user/avatar-station',

    /* 返回CODE */
    'CODE_SUCC'     : 0,
    'CODE_ERROR'    : 10000,
    'CODE_FAIL'     : 10001,
    'CODE_VALID'    : 10002,

    /* 类型 */
    'TYPE_CHANNEL'  : 1,

    /* Event */
    'EVT_USER_IDF'  : '_user_idf_'

});