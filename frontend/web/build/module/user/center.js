define(['app', 'vue', 'CONST', 'EVENT', 'CONFIG'], function(app, Vue, CONST, EVENT, CONFIG) {

    function UserCenter() {
        var $this, $main;
        var _page;

        this.export = function() {
            $this = this;
            return {
                init: function() {
                    $this.init();
                }
            };
        };
        this.init = function() {
            _page = $(app.$f7.getCurrentView().activePage.container);
            app.$mains.push(this.initVue().init());
        };
        this.initVue = function() {
            $main = new Vue({
                el: _page.find('.vue-app').get(0),
                data: function() {
                    return {
                        switchFlag  : false,
                        switchStat  : false,
                        isLogin     : false,
                        userInfo    : {
                            id      : 0,
                            avatar  : '',
                            username: '',
                            phone   : '',
                            sex     : 0,
                            time    : ''
                        }
                    };
                },
                mounted: function() {},
                methods: {
                    init: function() {
                        EVENT.on(CONST.EVT_USER_IDF, function() {
                            $main.isLogin = app.$user.isLogin;
                        }, true);
                        /* 加载用户资料 */
                        this.loadUser();
                        /* 显示页面 */
                        $(this.$el).show();

                        return $main;
                    },
                    handleAvatarChange: function(file) {
                        /* 限制文件上传大小为1MB以内 */
                        if (file.size > 1048576) {
                            app.$f7.alert('图片大小超过1MB！');
                            return;
                        }
                        app.loadPopup(CONST.POPUP_USER_AVATAR_STATION, {file: file, ctx: $main});
                    },
                    handleAvatarSuccess: function() {},
                    beforeAvatarUpload: function() {},
                    loadUser: function() {
                        app.GET(CONFIG.GET_USER_INFO).done(function(data) {
                            $main.userInfo = data;
                        });
                    },
                    switchBtns: function() {
                        this.switchStat = !this.switchStat;
                        if (!this.switchFlag) {
                            $main.switchFlag = !$main.switchFlag;
                            $(this.$refs.btnSwitch.$el).velocity({
                                'paddingRight'      : '1rem',
                                'backgroundColor'   : '#eebe2a',
                                'borderColor'       : '#eebe2a'
                            }, {
                                duration: 300
                            });
                        } else {
                            $(this.$refs.btnSwitch.$el).velocity('reverse', {
                                duration: 300,
                                complete: function() {
                                    $main.switchFlag = !$main.switchFlag;
                                }
                            });
                        }
                    },
                    logout: function() {
                        _modal = $(app.$f7.modal({
                            title: '退出',
                            text: '<span class="size-36"><i class="fa fa-frown-o"></i>&nbsp;要走了？</span>',
                            buttons: [
                                {
                                    text: '注销',
                                    onClick: function() {
                                        app.$user.logout().done(function() {
                                            /* 注销成功时进行下一步渲染 */
                                            app.loadPage({
                                                url     : CONST.PAGE_HOME,
                                                view    : CONST.MAIN_VIEW,
                                                reload  : true
                                            });
                                        }).fail(function(msg) {
                                            app.$f7.alert(msg);
                                        });
                                    }
                                },
                                {
                                    text: '留下',
                                    onClick: function() {}
                                }
                            ]
                        }));
                        _modal.find('.modal-button:first-child').addClass('color-7');
                        _modal.find('.modal-button:last-child').addClass('color-1');
                    }
                }
            });

            return $main;
        };
    }

    return (new UserCenter()).export();
});