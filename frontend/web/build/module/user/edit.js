define(['app', 'vue', 'CONST', 'EVENT', 'CONFIG'], function(app, Vue, CONST, EVENT, CONFIG) {

    function UserEdit() {
        var $this, $main;
        var _page;

        this.export = function() {
            $this = this;
            return {
                init: function() {
                    $this.init();
                }
            };
        };
        this.init = function() {
            _page = $(app.$f7.getCurrentView().activePage.container);
            app.$mains.push(this.initVue().init());
        };
        this.initVue = function() {
            $main = new Vue({
                el: _page.find('.vue-app').get(0),
                data: function() {
                    return {
                        editing     : false,
                        succNotice  : '',
                        errNotice   : '',
                        info        : {
                            username: '',
                            sex     : '0',
                            birthday: ''
                        }
                    };
                },
                mounted: function() {
                    Vue.nextTick(function() {
                        EVENT.on(CONST.EVT_USER_IDF, function() {
                            $main.info = app.$user.info();
                            if (!$main.info.birthday) $main.info.birthday = '2000-01-01';
                            _page.find('.picker-date').val($main.info.birthday);
                            app.$f7.picker({
                                input: '.page.user-edit .picker-date',
                                toolbar: false,
                                rotateEffect: true,
                                formatValue: function (p, values, displayValues) {
                                    var y = values[0].replace('年', '');
                                    var m = parseInt(values[1]);
                                    if (m < 10) m = '0'+m;
                                    var d = parseInt(values[2]);
                                    if (d < 10) d = '0'+d;

                                    return [y, m, d].join('-');
                                },
                                onOpen: function(picker) {
                                    var birthday = $main.info.birthday.split('-');
                                    var y = parseInt(birthday[0]);
                                    var m = parseInt(birthday[1]);
                                    var d = parseInt(birthday[2]);
                                    console.log([y, m, d]);
                                    picker.setValue([y+'年', m, d]);
                                },
                                onChange: function (picker, values) {
                                    var y = parseInt(values[0].replace('年', ''));
                                    var m = parseInt(values[1]);
                                    var d = parseInt(values[2]);
                                    var md= 30;
                                    /* 算出当前选中的月份最大天数 */
                                    if (m==1||m==3||m==5||m==7||m==8||m==10||m==12) {
                                        md = 31;
                                    } else if (m == 2) {
                                        md = 28;
                                        /* 判断是否闰年 */
                                        if (y % 4 === 0 ) {
                                            if (y % 100 === 0) {
                                                if (y % 400 === 0) {
                                                    md = 29;
                                                }
                                            } else {
                                                md = 29;
                                            }
                                        }
                                    }
                                    if (d > md) {
                                        picker.cols[2].setValue(md, 100);
                                    }
                                },
                                cols: [
                                    {
                                        values: (function () {
                                            var arr     = [];
                                            var maxY    = parseInt((new Date()).getFullYear());
                                            for (var i = 1950; i <= maxY; i++) { arr.push(i+'年'); }
                                            return arr;
                                        })()
                                    },
                                    {
                                        values: [1,2,3,4,5,6,7,8,9,10,11,12],
                                        displayValues: ('1月 2月 3月 4月 5月 6月 7月 8月 9月 10月 11月 12月').split(' ')
                                    },
                                    {
                                        values: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                                        displayValues: ['1日','2日','3日','4日','5日','6日','7日','8日','9日','10日','11日','12日','13日','14日','15日','16日','17日','18日','19日','20日','21日','22日','23日','24日','25日','26日','27日','28日','29日','30日','31日']
                                    }
                                ]
                            });
                        }, true);
                    });
                },
                methods: {
                    init: function() {
                        /* 显示页面 */
                        $(this.$el).show();

                        return $main;
                    },
                    edit: function() {
                        if ($main.editing) return;
                        $main.editing   = true;
                        $main.errNotice = '';

                        this.info.birthday = _page.find('.picker-date').val();
                        app.$user.edit($main.info).done(function() {
                            $main.succNotice = '修改完成';
                            setTimeout(function() {
                                $main.succNotice = '';
                            }, 2000);
                        }).fail(function() {
                            $main.errNotice = '修改失败';
                        }).always(function() {
                            $main.editing = false;
                        });
                    }
                }
            });

            return $main;
        };
    }

    return (new UserEdit()).export();
});