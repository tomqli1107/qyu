define(['vue', 'vuex', 'ELEMENT', 'CONST', 'PAGEFLASH', 'CONFIG', 'WEBSOCKET', 'USER', 'WEBSOCKETPOOL', 'WSACCEPTOR'], function(Vue, Vuex, ElementUI, CONST, PAGEFLASH, CONFIG, WEBSOCKET, USER, WEBSOCKETPOOL, WSACCEPTOR) {

    function App() {
        var $this, $export;

        this.export = function() {
            /* 设置$和$$ */
            window.$    = jQuery;
            window.$$   = Dom7;
            $this       = this;
            $export     = {
                $f7         : null,                 // f7框架对象
                $views      : {},                   // 保存f7的视图
                $app        : $this,                // app对象
                $history    : [],                   // 保存页面加载历史
                $connPool   : null,   // ws连接池
                $user       : null,            // 保存用户状态
                $wsAcceptor : null,
                $channels   : {},
                $mains      : [],
                flags       : {},
                _window     : $(window),
                __window    : $$(window),
                _body       : $('body'),
                __body      : $$('body'),
                GET: function(url, data) {
                    return $this.REQUEST(CONST.GET, url, data);
                },
                POST: function(url, data) {
                    return $this.REQUEST(CONST.POST, url, data);
                },
                init: function() {
                    $this.init();
                },
                renderBg: function(type) {
                    if (!$export.flags['bg_load']) {
                        $export.flags['bg_load'] = {};
                    }
                    type = type || CONST.BG_DEFAULT;
                    var _cx = $('.main-bg').get(0);
                    switch (type) {
                        case CONST.BG_DEFAULT:
                            var cb = function() {
                                var width, height, largeHeader, canvas, ctx, triangles, animateHeader = true;
                                var startPos    = {}, endPos = {};
                                //var colors      = ['238,180,34', '143,238,143', '28,134,238', '166,166,166', '224,33,48'];
                                var colors      = ['28,134,238', '43,81,102', '66,152,103', '250,178,67', '224,33,48'];
                                var maxNums     = 120;

                                // Main
                                initHeader();
                                addListeners();
                                initAnimation();

                                function initHeader() {
                                    width       = $export._window.width();
                                    height      = $export._window.height();
                                    startPos.x  = width*1;
                                    startPos.y  = height*1.2;
                                    endPos.x    = width*0.2;
                                    endPos.y    = height*0.5;

                                    canvas          = _cx;
                                    canvas.width    = width;
                                    canvas.height   = height-5;
                                    ctx = canvas.getContext('2d');

                                    // create particles
                                    triangles = [];
                                    for(var x = 0; x < maxNums; x++) {
                                        addTriangle(x*10);
                                    }
                                }

                                function addTriangle(delay) {
                                    setTimeout(function() {
                                        var t = new Triangle();
                                        triangles.push(t);
                                        tweenTriangle(t);
                                    }, delay);
                                }

                                function initAnimation() {
                                    animate();
                                }

                                function tweenTriangle(tri) {
                                    var t = Math.random()*(2*Math.PI);
                                    var x = (200+Math.random()*100)*Math.cos(t) + endPos.x;
                                    var y = (200+Math.random()*100)*Math.sin(t) + endPos.y;
                                    var time = 4+3*Math.random();

                                    TweenLite.to(tri.pos, time, {x: x,
                                        y: y, ease:Circ.easeOut,
                                        onComplete: function() {
                                            tri.init();
                                            tweenTriangle(tri);
                                        }});
                                }

                                // Event handling
                                function addListeners() {
                                    window.addEventListener('scroll', scrollCheck);
                                    window.addEventListener('resize', resize);
                                }

                                function scrollCheck() {
                                    animateHeader = document.body.scrollTop <= height;
                                }

                                function resize() {
                                    width           = $export._window.width();
                                    height          = $export._window.height();
                                    canvas.width    = width;
                                    canvas.height   = height-5;
                                }

                                function animate() {
                                    if(animateHeader) {
                                        ctx.clearRect(0,0,width,height);
                                        for(var i in triangles) {
                                            triangles[i].draw();
                                        }
                                    }
                                    requestAnimationFrame(animate);
                                }

                                // Canvas manipulation
                                function Triangle() {
                                    var _this = this;

                                    // constructor
                                    (function() {
                                        _this.coords = [{},{},{}];
                                        _this.pos = {};
                                        init();
                                    })();

                                    function init() {
                                        _this.pos.x = startPos.x;
                                        _this.pos.y = startPos.y;
                                        _this.coords[0].x = -10+Math.random()*40;
                                        _this.coords[0].y = -10+Math.random()*40;
                                        _this.coords[1].x = -10+Math.random()*40;
                                        _this.coords[1].y = -10+Math.random()*40;
                                        _this.coords[2].x = -10+Math.random()*40;
                                        _this.coords[2].y = -10+Math.random()*40;
                                        _this.scale = 0.1+Math.random()*0.3;
                                        _this.color = colors[Math.floor(Math.random()*colors.length)];
                                        setTimeout(function() { _this.alpha = 0.8; }, 10);
                                    }

                                    this.draw = function() {
                                        if(_this.alpha >= 0.006) _this.alpha -= 0.006;
                                        else _this.alpha = 0;
                                        ctx.beginPath();
                                        ctx.moveTo(_this.coords[0].x+_this.pos.x, _this.coords[0].y+_this.pos.y);
                                        ctx.lineTo(_this.coords[1].x+_this.pos.x, _this.coords[1].y+_this.pos.y);
                                        ctx.lineTo(_this.coords[2].x+_this.pos.x, _this.coords[2].y+_this.pos.y);
                                        ctx.closePath();
                                        ctx.fillStyle = 'rgba('+_this.color+','+ _this.alpha+')';
                                        ctx.fill();
                                    };

                                    this.init = init;
                                }
                            };
                            if (!$export.flags['bg_load'][CONST.BG_DEFAULT]) {
                                $.when($.getScript('/build/js/TweenLite.min.js'), $.getScript('/build/js/EasePack.min.js'), $.getScript('/build/js/rAF.js')).done(function() {
                                    $export.flags['bg_load'][CONST.BG_DEFAULT] = true;
                                    cb();
                                });
                            } else {
                                cb();
                            }
                            break;
                    }
                },
                loadPage: function(to, from) {
                    PAGEFLASH.from  = PAGEFLASH.from || [];
                    to.view = to.view || CONST.MAIN_VIEW;
                    /* 切换视图 */
                    if ($export.$f7.getCurrentView().selector != to.view)
                        $export.$f7.showTab(to.view);
                    /* 处理传参 */
                    if (from && from.url) {
                        from.view           = from.view || CONST.MAIN_VIEW;
                        from.query          = from.query ? $$.serializeObject(from.query) : null;
                        PAGEFLASH.from.push(encodeURIComponent(from.url+(from.query?'?'+from.query:'')));
                    } else {
                        var curUrl  = $export.$f7.getCurrentView().activePage.url,
                            view    = $export.getParameter(curUrl, 'view') || 'main';

                        PAGEFLASH.from.push(curUrl || 'page/home/index.html');
                    }
                    /* 加载 */
                    PAGEFLASH.url   = to.url;
                    PAGEFLASH.view  = to.view;

                    var loadOptions = {url: to.url, animatePages: true};
                    if (to.query) loadOptions.query = to.query;
                    loadOptions.reload = to.reload ? true : false;

                    $export.$views[to.view].router.load(loadOptions);
                },
                backPage: function(to) {
                    if (!to || !to.url) {
                        to = to || {};
                        if (PAGEFLASH.from && PAGEFLASH.from.length) {
                            to.url  = PAGEFLASH.from.pop();
                            //to.view = PAGEFLASH.fromView;
                        }
                    }
                    to.view = to.view || CONST.MAIN_VIEW;
                    if (!to.url) to.force = true;
                    to.url  = to.url || 'page/home/index.html';
                    if ($export.$f7.getCurrentView().selector != to.view) {
                        $export.$f7.showTab(to.view);
                        if (to.force) {
                            $export.$views[to.view].router.back({url: to.url, force: !!to.force, animatePages: true});
                        }
                    } else {
                        $export.$views[to.view].router.back({url: to.url, force: !!to.force, animatePages: true});
                    }
                    /* 执行回调 */
                    $export.$mains.pop();
                    var len = $export.$mains.length;
                    if (len) {
                        var $m = $export.$mains[len-1];
                        if ($m && $m.onBack) {
                            $m.onBack();
                        }
                    }
                },
                getParameter: function (url, name) {
                    $this.getParameter(url, name);
                },
                makeTalkEle: function(params) {

                    if (params.type == CONST.TALK_ELE_TYPE_TIME) {
                        return {
                            time    : params.time,
                            label   : params.timeDesc,
                            type    : CONST.TALK_ELE_TYPE_TIME,
                            loop    : params.loop,
                            isTime  : true
                        }
                    } else {
                        return {
                            user: {
                                id      : params.userId,
                                username: params.username,
                                avatar  : params.avatar
                            },
                            talk: {
                                content : params.content
                            },
                            time    : params.time,
                            type    : $export.$user.id == params.userId ? CONST.TALK_ELE_TYPE_SELF : CONST.TALK_ELE_TYPE_OTHER,
                            isTime  : false,
                            isSelf  : $export.$user.id == params.userId,
                            isOther : $export.$user.id != params.userId,
                            isSame  : !!params.isSame
                        };
                    }
                },
                makeMsgEle: function(type, params) {
                    switch (type) {
                        case CONST.WS_ELE_TYPE_TALK:
                        case CONST.WS_ELE_TYPE_REG:
                            params.type = type;
                            return params;
                            break;
                    }
                },
                loadPopup: function(name, data) {
                    var selector    = '.'+name.replace(/\//g,'-'),
                        url         = name+'.html',
                        _body       = $('body'),
                        $def        = $.Deferred();
                    $(selector).length && $(selector).remove();
                    $export.$f7.showIndicator();
                    $.ajax({url: url}).done(function(html) {
                        _body.append(html);
                        $export.$f7.popup(selector);
                        process();
                        $def.resolve();
                    });

                    function process() {
                        var module = $$(selector).data('name');
                        if (!module) {
                            $export.$f7.hideIndicator();
                            return;
                        }
                        require(['module/popup/' + module], function(mod) {
                            mod && mod.init && mod.init(data||{});
                            $export.$f7.hideIndicator();
                        });
                    }

                    return $def;
                },
                openSidebar: function(page) {
                    // $('.sidebar.sidebar-right').addClass('animated slideInRight').show();
                    /* 加载模块js */
                    // page.name && require(['module/' + page.name], function(mod) {
                    //     mod && mod.init && mod.init(page.query).done(function() {
                    //         $('.sidebar.sidebar-right').addClass('animated slideInRight');
                    //     });
                    // });
                },
                isIOS: function() {
                    return /\(i[^;]+;( U;)? CPU.+Mac OS X/i.test(navigator.userAgent);
                },
                isAndroid: function() {
                    return /(Android);?[\s\/]+([\d.]+)?/i.test(navigator.userAgent);
                },
                isWeiXin: function() {
                    return /MicroMessenger/i.test(navigator.userAgent);
                }
            };

            $export.$connPool   = new WEBSOCKETPOOL($export);
            $export.$user       = new USER($export);
            $export.$wsAcceptor = new WSACCEPTOR($export);

            return $export;
        };
        this.init = function() {
            /* 初始化 */
            this.startup();
            this.pageFit(document, window, 750, 320, 100);
            this.initFramework7();
            this.initVue();
            this.initAjax();
            this.initPushState();
            /* 渲染背景 */
            $export.renderBg();
            /* 设定页面js加载行为 */
            $$(document).on('pageBeforeInit', function (e) {
                var page = e.detail.page;
                /* 加载模块js */
                if (!$(page.container).hasClass('only-page')) {
                    page.name && require(['module/' + page.name], function(mod) {
                        mod && mod.init && mod.init(page.query);
                    });
                }
                /* 记录加载历史 */
                page.name && $export.$history.push({module: page.view.selector.match(/^#(.*)View$/)[1], page: page.url});
            });
            $$(document).on('pageBeforeRemove', function() {
                $export.$f7.initPageInfiniteScroll('.page');
            });
            /* 设定页面跳转的行为 */
            $(document).on('click', 'a[qyu-href]', function() {
                $export.loadPage({
                    url     : $(this).attr('qyu-href'),
                    view    : CONST.MAIN_VIEW
                });
            });
            /* 设定页面后退的行为 */
            $(document).on('click', '.qyu-back', function() {
                $export.backPage($(this).hasClass('force')?{force: true}:null);
            });
            /* 设定打开侧边栏的行为 */
            $(document).on('click', '.open-sidebar', function() {
                $export.openSidebar();
            });
            /* 默认启动 */
            this.loadPageFromUrl() || $$('#mainToolbar .tab-link.active').trigger('click');
        };
        this.pageFit = function(doc, win, maxwidth, minwidth, font) {
            var docEl   = doc.documentElement,
                recalc  = function () {
                    var clientWidth = docEl.clientWidth;
                    if (!clientWidth) return;
                    if (clientWidth >= minwidth && clientWidth <= maxwidth) {
                        docEl.style.fontSize = font * (clientWidth/maxwidth) + 'px';
                    } else if (clientWidth > maxwidth) {
                        docEl.style.fontSize = font + 'px';
                    } else if (clientWidth < minwidth) {
                        docEl.style.fontSize = font * (minwidth/maxwidth) + 'px';
                    }
                };
            recalc();
            if (!doc.addEventListener) return;
            win.addEventListener('orientationchange' in window ? 'orientationchange' : 'resize', recalc, false);
            doc.addEventListener('DOMContentLoaded', recalc, false);
        };
        this.initFramework7 = function() {
            $export.$f7 = new Framework7({
                pushState               : false,
                popupCloseByOutside     : false,
                animateNavBackIcon      : true,
                modalTitle              : '温馨提示',
                modalButtonOk           : '确定',
                modalButtonCancel       : '取消',
                swipeout                : false,
                animatePages            : !navigator.userAgent.match(/(Android);?[\s\/]+([\d.]+)?/),    // 安卓手机会出现切换黑屏
                preloadPreviousPage     : false,
                scrollTopOnNavbarClick  : true
            });
            $$('#mainToolbar .tab-link').each(function() {
                var url     = $$(this).attr('href');    // #mainView
                var name    = url.replace('#', '$');    // $mainView
                var prefix  = name.match(/^\$(.+)View$/)[1];     // mainView
                $export[name]               = $export.$f7.addView(url, {dynamicNavbar: true});
                $export.$views[url]         = $export[name];
                /* 添加视图名称到常量仓库 */
                CONST[[prefix, 'view'].join('_').toUpperCase()]  = url;
            });
        };
        this.initVue = function() {
            Vue.config.errorHandler = function (err, vm) {
                console.log('-------------vue error-------------');
                console.log(err);
                console.log(vm);
                console.log('-----------------------------------');
            };
            Vue.use(Vuex);
            Vue.use(ElementUI);
        };
        this.initAjax = function() {
            var options = {
                cache   : false,
                complete: function(xhr, status) {
                    status = xhr.status || status;
                    if (status == 403) {
                        // exports.loginRequired();
                    }
                    status != 200 && $export.$f7.hideIndicator();
                }
            };
            $.ajaxSetup(options);
            $$.ajaxSetup(options);
        };
        this.initPushState = function() {
            if (!('pushState' in history))
                return false;

            var isBacking = false;
            $$(window).on('popstate', function() {
                isBacking = true;
                $this.loadPageFromUrl();
            });
            $$(document).on('pageInit', function (e) {
                if (isBacking) {
                    isBacking = false;
                    return;
                }

                var page = e.detail.page, view = page.view;
                view && history.pushState(null, '', location.pathname + '?view='+view.selector.match(/^#(.*)View$/)[1]+'&page=' + encodeURIComponent(page.url));
            });
        };
        this.startup = function() {
            $export.GET('/site/startup').done(function(data) {
                console.log(data);
                $export.$user.identity(data);
            });
        };
        this.loadPageFromUrl = function() {
            var view    = this.getParameter(location.search, 'view') || 'main',
                page    = decodeURIComponent(this.getParameter(location.search, 'page') || 'page/home/index.html'),
                _view   = $$('.view-' + view);
            if (view && page && _view.length) {
                _view[0].f7View.router.load({url: page, reload: _view[0].f7View.url == page, animatePages: false});
                $export.$f7.showTab('#' + view + 'View');
                return true;
            }

            return false;
        };
        this.getParameter = function(url, name) {
            var r = new RegExp('(\\?|#|&)' + name + '=([^&#]*)(&|#|$)'),
                m = url.match(r);
            return (!m ? '' : m[2]);
        };
        this.REQUEST = function(type, url, data) {
            /* 参数设置 */
            type        = type || CONST.GET;
            data        = data || {};
            data.isAjax = 1;
            data.isWeb  = 1;
            /* 返回延迟对象 */
            var def = $.Deferred();

            $.ajax(url, {
                type    : type,
                data    : data,
                dataType: 'json'
            }).done(function(data) {
                if (data && !data.code) {
                    def.resolve(data.data, data.login);
                } else {
                    def.reject({
                        type    : CONST.EXCEPTION,
                        code    : data.code,
                        message : data.message||''
                    });
                }
            }).fail(function(xhr, errMsg, errThr) {
                def.reject({
                    type    : CONST.ERROR,
                    code    : xhr.status,
                    message : errThr||'系统错误'
                });
            });

            return def.promise();
        };
    }

    return (new App()).export();
});