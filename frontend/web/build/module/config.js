define({
    'WS_HOST'       : 'ws://192.168.1.102',
    'WS_PORT'       : '20001',
    'WS_SRV'        : 'ws://192.168.1.139:20001',

    /* ws连接池 */
    'MAX_POOL_SIZE' : 5,

    /* 请求路径 GET */
    'GET_CHANNEL_INFO'  : '/channel/detail',
    'GET_USER_INFO'     : '/user/detail',
    'GET_CHAT_MEMBERS'  : '/chat/members',
    /* 请求路径 POST */
    'POST_SEARCH'               : '/search',
    'POST_PHONE_SEND_SMS_CODE'  : '/phone/send-sms-code',
    'POST_REG_PHONE'            : '/reg/phone',
    'POST_LOGIN'                : '/site/login',
    'POST_LOGOUT'               : '/site/logout',
    'POST_CHATLOG'              : '/chat/log',
    'POST_UPLOAD_AVATAR'        : '/upload/avatar',
    'POST_USER_EDIT'            : '/user/edit'
});