define(['CONFIG', 'CONST'], function(CONFIG, CONST) {

    function WEBSOCKET(wsSrv, $acceptor) {

        wsSrv           = wsSrv || [CONFIG.WS_HOST, CONFIG.WS_PORT];
        this.$conn;
        this.connected  = false;
        this.url        = 'ws://' + wsSrv.join(':');
        this.host       = wsSrv[0];
        this.port       = wsSrv[1];

        this.open = function() {
            var $this   = this;
            var def     = $.Deferred();

            if (this.connected) {
                def.resolve();
                return def.promise();
            }

            try {
                this.$conn          = new WebSocket(this.url);
                this.$conn.onopen   = function() {
                    $this.connected = true;
                    def.resolve();
                    console.log($this.url + '连接成功');
                    $this.$conn.onmessage = function(msgEv) {
                        if ($acceptor) {
                            $acceptor.deal(msgEv);
                        }
                    };
                    $this.$conn.onclose = function(ev) {
                        $this.connected = false;
                        $this.$conn     = null;
                        if ($acceptor) {
                            $acceptor.dealClose(ev);
                        }
                    };
                    $this.$conn.onerror = function(er) {
                        def.reject(er);
                        if ($acceptor) {
                            $acceptor.dealError(er);
                        }
                    };
                };

                return def.promise();
            } catch(ex) {
                def.reject(ex);
            }

            def.reject();

            return def.promise();
        };
        this.send = function(obj) {
            if (this.$conn && this.connected) {
                obj = JSON.stringify(obj);
                if (obj) {
                    return this.$conn.send(obj);
                }
            }

            return false;
        };
        this.close = function() {
            console.log('关闭中...');
            if (this.$conn && this.connected) {
                this.$conn.close();
                this.connected  = false;    // 标记连接关闭
                this.$conn      = null;     // 释放内存
                console.log('主动关闭WS连接');
            }
        };
        this.reopen = function() {
            this.open();
            console.log('重新打开WS连接');
        };
    }

    return WEBSOCKET;
});