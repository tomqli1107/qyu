define(function() {

    function EVENT() {
        this._count = {};
        this._map   = {};

        this.on = function(type, cb, dealPrev) {
            if (!this._map[type]) {
                this._map[type]     = [];
                this._count[type]   = 0;
            }
            this._map[type].push(cb);
            if (dealPrev && this._count[type] && cb) {
                this.deal(cb);
            }
        };

        this.emit = function(type) {
            if (this._map[type]) {
                var $this = this;
                this._count[type]++;
                this._map[type].forEach(function(cb) {
                    $this.deal(cb);
                });
            }
        };

        this.deal = function(cb) {
            if (cb) {
                try {
                    cb();
                } catch (e) {
                    console.log(e);
                }
            }
        }
    }

    return new EVENT();
});