define(['CONFIG', 'WEBSOCKET', 'CONST'], function(CONFIG, WEBSOCKET, CONST) {

    function WEBSOCKETPOOL(app) {
        var $this   = this;

        this.$pool          = [];
        this.chanSrvMap     = {};   // 频道id与服务器id的映射
        this.srvMap         = {};   // 服务器id与ws连接对象的映射

        /* 为频道分配一个ws连接 */
        this.dispatch = function(channelId) {
            return _get(channelId);
        };

        /* 提交消息到ws服务器 */
        this.commit = function(msgEle) {
            if (msgEle && msgEle.type) {
                switch (msgEle.type) {
                    case CONST.WS_ELE_TYPE_REG:
                    case CONST.WS_ELE_TYPE_TALK:
                        var $conn = this.srvMap[this.chanSrvMap[msgEle.channelId]];
                        if ($conn && $conn.connected) {
                            return $conn.send(msgEle);
                        }
                        break;
                    case CONST.WS_ELE_TYPE_CHANNEL:
                        this.$pool.forEach(function(vConnEle) {
                            if (vConnEle.$conn && vConnEle.$conn.connected) {
                                vConnEle.$conn.send(msgEle);
                            }
                        });
                        break;
                }
            }

            return false;
        };

        function _get(channelId) {
            channelId = channelId || 0;

            if (!channelId)
                return null;

            var $conn = $this.srvMap[$this.chanSrvMap[channelId]||0];

            return ($conn && $conn.connected) ? $conn : _set(channelId);
        }

        function _set(channelId) {
            /* 获取频道的明细 */
            var $channel  = app.$channels[channelId];
            var serverId    = $channel.serverId();
            /* 建立频道与服务器的映射 */
            $this.chanSrvMap[channelId] = serverId;
            /* 根据serverId查看ws连接是否已经在连接池中 */
            if ($this.$pool.length >= CONFIG.MAX_POOL_SIZE) {
                var isIn = false;
                $this.$pool.forEach(function(vConnEle) {
                    if (vConnEle.serverId == serverId) {
                        isIn = true;
                    }
                });
                if (!isIn) {
                    var delConnEle = $this.$pool.shift();
                    delConnEle.$conn.close();                   // 关闭连接
                    $this.srvMap[delConnEle.serverId] = null;   // 解除映射
                }
            }
            /* 根据serverId查看ws连接是否已经创建 */
            if ($this.srvMap[serverId] && $this.srvMap[serverId].connected) {
                return $this.srvMap[serverId];
            }
            /* 创建新的连接 */
            var $conn       = new WEBSOCKET($channel.server().url.split(':'), app.$wsAcceptor);
            var connEle     = {
                'serverId'  : serverId,
                '$conn'     : $conn
            };
            $this.$pool.push(connEle);
            $this.srvMap[serverId] = $conn;

            return $conn;
        }
    }

    return WEBSOCKETPOOL;
});