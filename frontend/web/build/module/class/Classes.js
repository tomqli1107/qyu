define('EVENT', function() {

    function EVENT() {
        this._count = {};
        this._map   = {};

        this.on = function(type, cb, dealPrev) {
            if (!this._map[type]) {
                this._map[type]     = [];
                this._count[type]   = 0;
            }
            this._map[type].push(cb);
            if (dealPrev && this._count[type] && cb) {
                this.deal(cb);
            }
        };

        this.emit = function(type) {
            if (this._map[type]) {
                var $this = this;
                this._count[type]++;
                this._map[type].forEach(function(cb) {
                    $this.deal(cb);
                });
            } else {
                this._map[type]     = [];
                this._count[type]   = 1;
            }
        };

        this.deal = function(cb) {
            if (cb) {
                try {
                    cb();
                } catch (e) {
                    console.log(e);
                }
            }
        }
    }

    return new EVENT();
});
define('CHANNEL', ['CONST'], function(CONST) {

    function CHANNEL(detail, app) {

        this.$connPool  = app.$connPool;
        this.detail     = detail;

        /* 发送消息 */
        this.send = function(ele) {
            /* 提交到服务器 */
            return this.$connPool.commit(app.makeMsgEle(ele.type, {
                userId      : ele.user.id,
                username    : ele.user.username,
                avatar      : ele.user.avatar,
                channelId   : this.detail.id,
                content     : ele.talk.content
            }));
        };

        /* 进入频道 */
        this.join = function() {
            var $this = this;
            /* 分配一个连接 */
            var $conn = this.$connPool.dispatch(this.detail.id);
            /* 打开连接 */
            $conn.open().done(function() {
                $this.register();
            }).fail(function(er) {
                console.log(er);
            });
        };

        /* 登记用户和转换频道信息 */
        this.register = function() {
            this.$connPool.commit(app.makeMsgEle(CONST.WS_ELE_TYPE_REG, {
                userId      : app.$user.id,
                channelId   : this.detail.id
            }));
            this.$connPool.commit(app.makeMsgEle(CONST.WS_ELE_TYPE_CHANNEL, {
                userId      : app.$user.id,
                channelId   : this.detail.id
            }));
        };

        this.channelId = function() {
            return this.detail.id;
        };

        this.serverId = function() {
            return this.detail.server.id;
        };

        this.server = function() {
            return this.detail.server;
        };

    };

    return CHANNEL;
});
define('USER', ['CONFIG', 'CHANNEL', 'CONST', 'EVENT'], function(CONFIG, CHANNEL, CONST, EVENT) {

    function USER(app) {

        this.id         = 0;
        this.username   = '';
        this.phone      = '';
        this.sex        = 0;
        this.birthday   = '';
        this.avatar     = '';
        this.isLogin    = false;
        this.channelId  = 1;
        this.type       = 0;
        this.typeId     = 0;
        this.searchChannelMap  = {};

        this.logout = function() {
            var $def = $.Deferred();
            app.POST(CONFIG.POST_LOGOUT).done(function(data) {
                app.$user.identity(data);
                $def.resolve();
            }).fail(function(er) {
                $def.reject(er.message);
            });

            return $def;
        };

        this.info = function() {
            return {
                id      : this.id,
                username: this.username,
                phone   : this.phone,
                sex     : this.sex,
                birthday: this.birthday,
                avatar  : this.avatar
            };
        };

        /* 获取身份标识 */
        this.identity = function(data) {
            if (!data) return;

            this.id     = data.id ? data.id : (new Date()).getTime();
            this.avatar = data.avatar;
            this.isLogin= data.isLogin;
            if (data && data.isLogin) {
                this.username   = data.username;
                this.phone      = data.phone;
                this.sex        = data.sex;
                this.birthday   = data.birthday;
            } else {
                this.username   = '游客' + this.id;
                this.phone      = '';
                this.sex        = 0;
                this.birthday   = '';
                this.isLogin    = false;
            }
            EVENT.emit(CONST.EVT_USER_IDF);
        };

        /* 搜索频道 */
        this.search = function(input, type) {
            type = type || CONST.TYPE_CHANNEL;
            var $this   = this;
            var def     = $.Deferred();
            switch (type) {
                case CONST.TYPE_CHANNEL:
                    if (this.searchChannelMap[input]) def.resolve(this.searchChannelMap[input]);
                    else {
                        app.POST(CONFIG.POST_SEARCH, {input: input, type: type}).done(function(data) {
                            $this.searchChannelMap[input] = data;
                            def.resolve(data);
                        }).fail(function() {
                            def.reject();
                        });
                    }
                    break;
            }

            return def.promise();
        };


        /* 进入频道 */
        this.channel = function(channelId) {
            channelId       = parseInt(channelId) || 0;
            this.channelId  = channelId;
            this.type       = CONST.TYPE_CHANNEL;
            this.typeId     = channelId;
            var def         = $.Deferred();
            if (this.channelId) {
                /* 获取channel的信息 */
                if (!app.$channels[channelId]) {
                    app.GET(CONFIG.GET_CHANNEL_INFO, {id: channelId}).done(function(channelDtl) {
                        app.$channels[channelId] = new CHANNEL(channelDtl, app);
                        app.$channels[channelId].join();
                        def.resolve();
                    }).fail(function() {
                        def.reject();
                    });
                } else {
                    app.$channels[channelId].join();
                    def.resolve();
                }
            } else {
                this.channelId  = 0;
                this.type       = 0;
                this.typeId     = 0;
                def.reject();
            }

            return def.promise();
        };

        /* 说话 */
        this.talk = function(talkInput) {
            /* 提交到服务器 */
            if (talkInput && this.typeId) {
                /* 保存聊天记录 */
                var data = {
                    user_id     : this.id,
                    visitor     : this.isLogin ? 0 : 1,
                    content     : talkInput,
                    type_id     : this.typeId,
                    type        : this.type
                };
                app.POST(CONFIG.POST_CHATLOG, data);
                return app.$channels[this.channelId].send(app.makeTalkEle({
                    userId  : this.id,
                    username: this.username,
                    avatar  : this.avatar,
                    content : talkInput,
                    isNew   : true
                }));
            }

            return false;
        };

        /* 编辑 */
        this.edit = function(info) {
            var $def = $.Deferred();
            var data = {
                username: info.username,
                sex     : info.sex,
                birthday: info.birthday
            };
            app.POST(CONFIG.POST_USER_EDIT, data).done(function() {
                app.$user.username  = info.username;
                app.$user.sex       = info.sex;
                app.$user.birthday  = info.birthday;
                $def.resolve();
            }).fail(function() {
                $def.reject();
            });

            return $def;
        }
    }

    return USER;
});
define('WEBSOCKET', ['CONFIG', 'CONST'], function(CONFIG, CONST) {

    function WEBSOCKET(wsSrv, $acceptor) {

        wsSrv           = wsSrv || [CONFIG.WS_HOST, CONFIG.WS_PORT];
        this.$conn;
        this.connected  = false;
        this.url        = 'ws://' + wsSrv.join(':');
        this.host       = wsSrv[0];
        this.port       = wsSrv[1];

        this.open = function() {
            var $this   = this;
            var def     = $.Deferred();

            if (this.connected) {
                def.resolve();
                return def.promise();
            }

            try {
                this.$conn          = new WebSocket(this.url);
                this.$conn.onopen   = function() {
                    $this.connected = true;
                    def.resolve();
                    console.log($this.url + '连接成功');
                    $this.$conn.onmessage = function(msgEv) {
                        if ($acceptor) {
                            $acceptor.deal(msgEv);
                        }
                    };
                    $this.$conn.onclose = function(ev) {
                        $this.connected = false;
                        $this.$conn     = null;
                        if ($acceptor) {
                            $acceptor.dealClose(ev);
                        }
                    };
                    $this.$conn.onerror = function(er) {
                        def.reject(er);
                        if ($acceptor) {
                            $acceptor.dealError(er);
                        }
                    };
                };

                return def.promise();
            } catch(ex) {
                def.reject(ex);
            }

            def.reject();

            return def.promise();
        };
        this.send = function(obj) {
            if (this.$conn && this.connected) {
                obj = JSON.stringify(obj);
                if (obj) {
                    return this.$conn.send(obj);
                }
            }

            return false;
        };
        this.close = function() {
            console.log('关闭中...');
            if (this.$conn && this.connected) {
                this.$conn.close();
                this.connected  = false;    // 标记连接关闭
                this.$conn      = null;     // 释放内存
                console.log('主动关闭WS连接');
            }
        };
        this.reopen = function() {
            this.open();
            console.log('重新打开WS连接');
        };
    }

    return WEBSOCKET;
});
define('WEBSOCKETPOOL', ['CONFIG', 'WEBSOCKET', 'CONST'], function(CONFIG, WEBSOCKET, CONST) {

    function WEBSOCKETPOOL(app) {
        var $this   = this;

        this.$pool          = [];
        this.chanSrvMap     = {};   // 频道id与服务器id的映射
        this.srvMap         = {};   // 服务器id与ws连接对象的映射

        /* 为频道分配一个ws连接 */
        this.dispatch = function(channelId) {
            return _get(channelId);
        };

        /* 提交消息到ws服务器 */
        this.commit = function(msgEle) {
            if (msgEle && msgEle.type) {
                switch (msgEle.type) {
                    case CONST.WS_ELE_TYPE_REG:
                    case CONST.WS_ELE_TYPE_TALK:
                        var $conn = this.srvMap[this.chanSrvMap[msgEle.channelId]];
                        if ($conn && $conn.connected) {
                            return $conn.send(msgEle);
                        }
                        break;
                    case CONST.WS_ELE_TYPE_CHANNEL:
                        this.$pool.forEach(function(vConnEle) {
                            if (vConnEle.$conn && vConnEle.$conn.connected) {
                                vConnEle.$conn.send(msgEle);
                            }
                        });
                        break;
                }
            }

            return false;
        };

        function _get(channelId) {
            channelId = channelId || 0;

            if (!channelId)
                return null;

            var $conn = $this.srvMap[$this.chanSrvMap[channelId]||0];

            return ($conn && $conn.connected) ? $conn : _set(channelId);
        }

        function _set(channelId) {
            /* 获取频道的明细 */
            var $channel  = app.$channels[channelId];
            var serverId    = $channel.serverId();
            /* 建立频道与服务器的映射 */
            $this.chanSrvMap[channelId] = serverId;
            /* 根据serverId查看ws连接是否已经在连接池中 */
            if ($this.$pool.length >= CONFIG.MAX_POOL_SIZE) {
                var isIn = false;
                $this.$pool.forEach(function(vConnEle) {
                    if (vConnEle.serverId == serverId) {
                        isIn = true;
                    }
                });
                if (!isIn) {
                    var delConnEle = $this.$pool.shift();
                    delConnEle.$conn.close();                   // 关闭连接
                    $this.srvMap[delConnEle.serverId] = null;   // 解除映射
                }
            }
            /* 根据serverId查看ws连接是否已经创建 */
            if ($this.srvMap[serverId] && $this.srvMap[serverId].connected) {
                return $this.srvMap[serverId];
            }
            /* 创建新的连接 */
            var $conn       = new WEBSOCKET($channel.server().url.split(':'), app.$wsAcceptor);
            var connEle     = {
                'serverId'  : serverId,
                '$conn'     : $conn
            };
            $this.$pool.push(connEle);
            $this.srvMap[serverId] = $conn;

            return $conn;
        }
    }

    return WEBSOCKETPOOL;
});
define('WSACCEPTOR', ['CONST'], function(CONST) {

    function WSACCEPTOR(app) {

        this.cbMap = {};

        this.deal = function(msgEv) {
            console.log(msgEv);
            var receive = JSON.parse(msgEv.data);

            if (this.cbMap[receive.type]) {
                this.cbMap[receive.type].call(null, receive);
            }
        };

        this.dealClose = function(ev) {
            console.log(ev);
            if (ev.code == CONST.WS_CLOSE_CODE_TIMEOUT) {
                /* 超时重连 */
                app.$user.channel(app.$user.channelId);
            }
        };

        this.callback = function(type, cb) {
            this.cbMap[type] = cb;
        };
    };

    return WSACCEPTOR;
});