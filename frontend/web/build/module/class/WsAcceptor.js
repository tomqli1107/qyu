define(['CONST'], function(CONST) {

    function WSACCEPTOR(app) {

        this.cbMap = {};

        this.deal = function(msgEv) {
            console.log(msgEv);
            var receive = JSON.parse(msgEv.data);

            if (this.cbMap[receive.type]) {
                this.cbMap[receive.type].call(null, receive);
            }
        };

        this.dealClose = function(ev) {
            console.log(ev);
            if (ev.code == CONST.WS_CLOSE_CODE_TIMEOUT) {
                /* 超时重连 */
                app.$user.channel(app.$user.channelId);
            }
        };

        this.callback = function(type, cb) {
            this.cbMap[type] = cb;
        };
    };

    return WSACCEPTOR;
});