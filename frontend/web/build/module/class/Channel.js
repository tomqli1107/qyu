define(['CONST'], function(CONST) {

    function CHANNEL(detail, app) {

        this.$connPool  = app.$connPool;
        this.detail     = detail;

        /* 发送消息 */
        this.send = function(ele) {
            /* 提交到服务器 */
            return this.$connPool.commit(app.makeMsgEle(ele.type, {
                userId      : ele.user.id,
                channelId   : this.detail.id,
                content     : ele.talk.content
            }));
        };

        /* 进入频道 */
        this.join = function() {
            var $this = this;
            /* 分配一个连接 */
            var $conn = this.$connPool.dispatch(this.detail.id);
            /* 打开连接 */
            $conn.open().done(function() {
                $this.register();
            }).fail(function(er) {
                console.log(er);
            });
        };

        /* 登记用户和转换频道信息 */
        this.register = function() {
            this.$connPool.commit(app.makeMsgEle(CONST.WS_ELE_TYPE_REG, {
                userId      : app.$user.id,
                channelId   : this.detail.id
            }));
            this.$connPool.commit(app.makeMsgEle(CONST.WS_ELE_TYPE_CHANNEL, {
                userId      : app.$user.id,
                channelId   : this.detail.id
            }));
        };

        this.channelId = function() {
            return this.detail.id;
        };

        this.serverId = function() {
            return this.detail.server.id;
        };

        this.server = function() {
            return this.detail.server;
        };

    };

    return CHANNEL;
});
