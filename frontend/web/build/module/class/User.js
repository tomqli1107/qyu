define(['CONFIG', 'CHANNEL', 'CONST', 'EVENT'], function(CONFIG, CHANNEL, CONST, EVENT) {

    function USER(app) {

        this.id         = 0;
        this.username   = '';
        this.phone      = '';
        this.avatar     = '';
        this.isLogin    = false;
        this.channelId  = 1;
        this.topicId    = 0;
        this.roomId     = 1;
        this.searchChannelMap  = {};

        /* 获取身份标识 */
        this.identity = function(data) {
            if (!data) return;

            this.id     = data.id ? data.id : (new Date()).getTime();
            this.isLogin= data.isLogin;
            if (data && data.isLogin) {
                this.username   = data.username;
                this.phone      = data.phone;
                this.avatar     = data.avatar;
            } else {
                this.username   = '';
                this.phone      = '';
                this.avatar     = '';
                this.isLogin    = false;
            }
            EVENT.emit(CONST.EVT_USER_IDF);
        };

        /* 搜索频道 */
        this.search = function(input, type) {
            type = type || CONST.TYPE_CHANNEL;
            var $this   = this;
            var def     = $.Deferred();
            switch (type) {
                case CONST.TYPE_CHANNEL:
                    if (this.searchChannelMap[input]) def.resolve(this.searchChannelMap[input]);
                    else {
                        app.POST(CONFIG.POST_SEARCH, {input: input, type: type}).done(function(data) {
                            $this.searchChannelMap[input] = data;
                            def.resolve(data);
                        }).fail(function() {
                            def.reject();
                        });
                    }
                    break;
            }

            return def.promise();
        };


        /* 进入频道 */
        this.channel = function(channelId) {
            channelId       = parseInt(channelId) || 0;
            this.channelId  = channelId;
            var def         = $.Deferred();
            if (this.channelId) {
                /* 获取channel的信息 */
                if (!app.$channels[channelId]) {
                    app.GET(CONFIG.GET_CHANNEL_INFO, {id: channelId}).done(function(channelDtl) {
                        app.$channels[channelId] = new CHANNEL(channelDtl, app);
                        app.$channels[channelId].join();
                        def.resolve();
                    }).fail(function() {
                        def.reject();
                    });
                } else {
                    app.$channels[channelId].join();
                    def.resolve();
                }
            } else {
                this.channelId = 0;
                def.reject();
            }

            return def.promise();
        };

        /* 说话 */
        this.talk = function(talkInput) {
            /* 提交到服务器 */
            if (talkInput && this.channelId) {
                return app.$channels[this.channelId].send(app.makeTalkEle({
                    userId  : this.id,
                    content : talkInput,
                    isNew   : true
                }));
            }

            return false;
        };
    };

    return USER;
});