define(['app', 'vue', 'CONST', 'EVENT', 'CONFIG'], function(app, Vue, CONST, EVENT, CONFIG) {

    function HomeIndex() {
        var $this, $main;
        var _page;

        this.export = function() {
            $this = this;
            return {
                init: function() {
                    $this.init();
                }
            };
        };
        this.init = function() {
            _page = $(app.$f7.getCurrentView().activePage.container);
            app.$mains.push(this.initVue().init());
        };
        this.initVue = function() {
            $main = new Vue({
                el: _page.find('.vue-app').get(0),
                data: function() {
                    return {
                        switchFlag  : false,
                        switchStat  : false,
                        idf         : false,
                        isLogin     : false,
                        userInfo    : {
                            username: '',
                            avatar  : ''
                        },
                        searchInput : ''
                    };
                },
                methods: {
                    init: function() {
                        EVENT.on(CONST.EVT_USER_IDF, function() {
                            $main.idf               = true;
                            $main.userInfo.username = app.$user.username;
                            $main.userInfo.avatar   = app.$user.avatar;
                            $main.isLogin           = app.$user.isLogin;
                        }, true);
                        setTimeout(function() {
                            $($main.$el).show();
                        }, 100);

                        return $main;
                    },
                    start: function(channelId) {
                        app.loadPage({
                            url     : CONST.PAGE_CHANNEL+'?id='+channelId,
                            view    : CONST.MAIN_VIEW
                        });
                    },
                    search: function() {
                        if (this.searchInput) {
                            app.$user.search(this.searchInput).done(function(data) {
                                if (data.channelId) {
                                    app.loadPage({
                                        url     : CONST.PAGE_CHANNEL+'?id='+data.channelId,
                                        view    : CONST.MAIN_VIEW
                                    });
                                } else {
                                    app.$f7.alert('出错啦~');
                                }
                            }).fail(function() {
                                app.$f7.alert('出错啦~');
                            }).always(function() {
                                $main.searchInput = '';
                            });
                        }
                    },
                    register: function() {
                        app.loadPage({
                            url     : CONST.PAGE_REG_PHONE_1,
                            view    : CONST.MAIN_VIEW
                        });
                    },
                    switchBtns: function() {
                        this.switchStat = !this.switchStat;
                        if (!this.switchFlag) {
                            $main.switchFlag = !$main.switchFlag;
                            $(this.$refs.btnSwitch.$el).velocity({
                                'paddingRight'      : '1rem',
                                'backgroundColor'   : '#eebe2a',
                                'borderColor'       : '#eebe2a'
                            }, {
                                duration: 300
                            });
                        } else {
                            $(this.$refs.btnSwitch.$el).velocity('reverse', {
                                duration: 300,
                                complete: function() {
                                    $main.switchFlag = !$main.switchFlag;
                                }
                            });
                        }
                    },
                    logout: function() {
                        _modal = $(app.$f7.modal({
                            title: '退出',
                            text: '<span class="size-36"><i class="fa fa-frown-o"></i>&nbsp;要走了？</span>',
                            buttons: [
                                {
                                    text: '注销',
                                    onClick: function() {
                                        app.$user.logout().done(function() {
                                            /* 注销成功时进行下一步渲染 */
                                            app.loadPage({
                                                url     : CONST.PAGE_HOME,
                                                view    : CONST.MAIN_VIEW,
                                                reload  : true
                                            });
                                        }).fail(function(msg) {
                                            app.$f7.alert(msg);
                                        });
                                    }
                                },
                                {
                                    text: '留下',
                                    onClick: function() {}
                                }
                            ]
                        }));
                        _modal.find('.modal-button:first-child').addClass('color-7');
                        _modal.find('.modal-button:last-child').addClass('color-1');
                    }
                }
            });

            return $main;
        };
    }

    return (new HomeIndex()).export();
});