define(['app', 'vue', 'CONFIG', 'CROPPER'], function(app, Vue, CONFIG) {

    function AvatarStation() {
        var $this, $cropper, $ctrl, $ctx, $file;
        var _pop, _avatar;

        this.export = function() {
            $this = this;
            return {
                init: function(data) {
                    $file   = data['file'];
                    $ctx    = data['ctx'];
                    console.log($file);
                    $this.init();
                }
            };
        };
        this.init = function() {
            _pop = $('.popup.popup-user-avatar-station');
            this.initVue().init();
        };
        this.initVue = function() {
            $ctrl = new Vue({
                el: _pop.find('.ctrl').get(0),
                data: function() {
                    return {
                        uploading: false
                    };
                },
                mounted: function() {
                    _ctrl = $(this.$refs.ctrl);
                },
                methods: {
                    cancel: function() {
                        app.$f7.closeModal(_pop.get(0));
                    },
                    ok: function() {
                        if (this.uploading) return;
                        this.uploading = true;
                        app.$f7.showIndicator();
                        app.POST(CONFIG.POST_UPLOAD_AVATAR, {avatar: _avatar.cropper('getCroppedCanvas').toDataURL('image/png'), name: $file.name}).done(function(url) {
                            Vue.set($ctx.userInfo, 'avatar', url);
                            app.$f7.closeModal(_pop.get(0));
                        }).always(function() {
                            $ctrl.uploading = false;
                            app.$f7.hideIndicator();
                        });
                    }
                }
            });
            $cropper = new Vue({
                el: _pop.find('.cropper').get(0),
                data: function() {
                    return {
                        image: $file
                    };
                },
                mounted: function() {
                    _avatar = $(this.$refs.avatar);
                },
                methods: {
                    init: function() {
                        _avatar.cropper({
                            dragMode: 'move',
                            viewMode: 1,
                            aspectRatio: 1,
                            minContainerWidth: app._window.outerWidth(),
                            minContainerHeight: app._window.outerHeight(),
                            rotatable: false,
                            autoCropArea: 1,
                            cropBoxMovable: false,
                            cropBoxResizable: false
                        });
                    }
                }
            });

            return $cropper;
        };
    }

    return (new AvatarStation()).export();
});