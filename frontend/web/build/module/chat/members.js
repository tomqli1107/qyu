define(['app', 'vue', 'CONST', 'EVENT', 'CONFIG'], function(app, Vue, CONST, EVENT, CONFIG) {

    function ChatMembers() {
        var $this, $main;
        var _page;

        this.export = function() {
            $this = this;
            return {
                init: function() {
                    $this.init();
                }
            };
        };
        this.init = function() {
            _page = $(app.$f7.getCurrentView().activePage.container);
            app.$mains.push(this.initVue().init());
        };
        this.initVue = function() {
            $main = new Vue({
                el: _page.find('.vue-app').get(0),
                data: function() {
                    return {
                        members: []
                    };
                },
                mounted: function() {},
                methods: {
                    init: function() {
                        /* 显示页面 */
                        $(this.$el).show();
                        /* 加载成员 */
                        this.loadMembers();
                        /* 加载其他 */
                        this.loadDetais();

                        return $main;
                    },
                    loadMembers: function() {
                        app.GET(CONFIG.GET_CHAT_MEMBERS, {type: app.$user.type, type_id: app.$user.typeId}).done(function(members) {
                            $main.members = members;
                        });
                    },
                    loadDetais: function() {

                    }
                }
            });

            return $main;
        };
    }

    return (new ChatMembers()).export();
});