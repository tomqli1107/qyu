define(['app', 'vue', 'vuex', 'CONST'], function(app, Vue, Vuex, CONST) {

    function ChatIndex() {
        var $this, $export;
        var $main, $talkBar, $navBar, $store;
        var _page, _pageContent, _talkBar, _chatBox, _navBar, _chatBoard;
        var talkBarViewSeed, orgWH, orgCBTop;

        /* 频道id */
        var channelId = 0;

        this.export = function() {
            $this       = this;
            $export     = {
                init: function(query) {
                    $this.init(query)
                }
            };

            return $export;
        };
        this.init = function(query) {
            if (query && query.id) {
                channelId   = query.id;
                _page       = $(app.$f7.getCurrentView().activePage.container);
                orgWH       = app._window.outerHeight();
                app.$mains.push(this.initVue().init());
            }
        };
        this.initVue = function() {
            $store  = new Vuex.Store({
                state: {
                    channelId   : 0,
                    chatList    : [],
                    tmpChatList : [],
                    editing     : false
                },
                mutations: {
                    talk: function() {
                        $talkBar.talkInput = '';
                    },
                    receive: function(state, msgEle) {
                        if (state.channelId != msgEle.channelId) return;

                        $main.pushChatList(state.chatList, msgEle);

                        Vue.nextTick(function() {
                            (function() {
                                var count   = 0;
                                var seed    = setInterval(function() {
                                    ++count;
                                    if (count > 10) {
                                        _chatBoard.show();
                                        clearInterval(seed);
                                        return;
                                    }
                                    var boardH  = _chatBoard.outerHeight();
                                    var diffH   = _pageContent.height() - boardH;
                                    if (diffH > 0) {
                                        _chatBoard.hide();
                                        var curWH   = app._window.outerHeight() - 44;
                                        if (boardH < curWH) {
                                            diffH = orgWH - curWH - 44;
                                        } else {
                                            diffH -= 20;
                                        }
                                        _chatBoard.css('paddingTop', diffH+'px');
                                    }
                                }, 50);
                            })();

                            if (_chatBoard.outerHeight() < _pageContent.height()) return;

                            var addCount = function() {
                                if (msgEle.userId != app.$user.id)
                                    ++$main.unReadCount;
                            };
                            var offsetTop = _chatBoard.offset().top;
                            if (offsetTop >= orgCBTop) {
                                if (_chatBoard.outerHeight() > _pageContent.height() + 100) {
                                    addCount();
                                } else if (_chatBoard.outerHeight() > _pageContent.height()) {
                                    $main.scrollToChatBottom();
                                }
                            } else {
                                var offsetH = offsetTop > 0 ? offsetTop : (orgCBTop-offsetTop);
                                var maxH    = _chatBoard.outerHeight() - _pageContent.height();
                                if (offsetH + 100 > maxH) {
                                    $main.scrollToChatBottom();
                                } else {
                                    addCount();
                                }
                            }
                        });
                    },
                    initChat: function(state, params) {
                        state.channelId = params.channelId;
                        state.chatList  = params.chatLog || [];
                    },
                    fetchHistory: function(state, logs) {
                        ++$main.historyLoop;
                        state.tmpChatList = [];
                        var isInit = $main.historyLoop === 1;
                        logs.forEach(function(msgEle) {
                            if (!isInit) msgEle.loop = $main.historyLoop;
                            $main.pushChatList(isInit?state.chatList:state.tmpChatList, msgEle);
                        });
                        if (isInit) {
                            $main.scrollToChatBottom();
                        } else {
                            state.chatList = state.tmpChatList.concat(state.chatList);
                        }
                    }
                },
                actions: {
                    talk: function(ctx) {
                        _talkBar.find('.input-area textarea').focus();
                        if (!$talkBar.talkInput) return;
                        /* 执行说话 */
                        app.$user.talk($talkBar.talkInput);
                        ctx.commit(CONST.ACT_TALK);
                    },
                    receive: function(ctx, msgEle) {
                        ctx.commit(CONST.ACT_RECEIVE, msgEle);
                    },
                    initChat: function(ctx) {
                        $store.dispatch(CONST.ACT_FETCH_HISTORY);
                        ctx.commit(CONST.ACT_INIT_CHAT, {channelId: channelId});
                    },
                    fetchHistory: function(ctx) {
                        $main.fetchHistory($main.limitVal).done(function(logs) {
                            ctx.commit(CONST.ACT_FETCH_HISTORY, logs);
                        });
                    }
                }
            });
            $navBar = new Vue({
                el: _page.find('.nav-bar').get(0),
                data: function() {
                    return {
                        title: ''
                    };
                },
                mounted: function() {
                    _navBar = $(this.$el).show();
                }
            });
            $talkBar= new Vue({
                el: _page.find('.talk-bar').get(0),
                data: function() {
                    return {
                        talkInput   : '',
                        editing     : false
                    };
                },
                mounted: function() {
                    _talkBar = $(this.$el);
                },
                methods: {
                    talk: function() {
                        $main.$store.dispatch(CONST.ACT_TALK);
                    },
                    talkInputFocus: function() {
                        talkBarViewSeed = setInterval(function() {
                            _talkBar.get(0).scrollIntoView();
                        }, 100);
                        setTimeout(function() {
                            if (talkBarViewSeed) {
                                clearInterval(talkBarViewSeed);
                            }
                        }, 500);
                        setTimeout(function() {
                            var boardH  = _chatBoard.outerHeight();
                            var diffH   = _pageContent.height() - boardH;
                            if (diffH > 0) {
                                var curWH   = app._window.outerHeight() - 44;
                                if (boardH < curWH) {
                                    diffH = orgWH - curWH - 44;
                                } else {
                                    diffH -= 20;
                                }

                                _chatBoard.velocity({paddingTop: diffH+'px'}, {duration: 300});
                            } else {
                                $main.scrollToChatBottom();
                            }
                        }, 300);
                        this.editing = true;
                    },
                    talkInputBlur: function() {
                        if (talkBarViewSeed) {
                            clearInterval(talkBarViewSeed);
                        }
                        this.editing = false;
                        setTimeout(function() {
                            if (!$talkBar.editing) {
                                _chatBoard.velocity({paddingTop: 0}, {duration: 500});
                            }
                        }, 100);
                    }
                }
            });
            $main   = new Vue({
                el: _page.find('.vue-app').get(0),
                store: $store,
                computed: {
                    chatList: function() {
                        return this.$store.state.chatList;
                    }
                },
                data: function() {
                    return {
                        unReadCount : 0,
                        historyLoop : 0,
                        limitVal    : 0,
                        refresh     : false,
                        refreshDown : false,
                        debug       : ''
                    };
                },
                watch: {
                    unReadCount: function (val) {
                        if (val) {
                            _page.find('.receive-tip').show();
                        } else {
                            _page.find('.receive-tip').hide();
                        }
                    }
                },
                mounted: function() {
                    _pageContent= _page.find('.page-content');
                    _chatBox    = $(this.$refs.chatBox);
                    _chatBoard  = _chatBox.find('.chat-board');
                    orgCBTop    = _chatBoard.offset().top;
                },
                methods: {
                    init: function() {
                        /* 修正ChatBoard的高度 */
                        setTimeout(this.resizeChatBoxHeight, 100);
                        /* 设置下拉事件 */
                        $$(_pageContent.get(0)).on('refresh', function() {
                            if ($main.refresh) return;
                            $main.refresh = true;
                            $main.$store.dispatch(CONST.ACT_FETCH_HISTORY);
                        });
                        /* 设置touchmove事件 */
                        $$(_chatBoard.get(0)).touchmove(function() {
                            /* 关闭消息提示 */
                            $main.unReadCount = 0;
                            /* 关闭输入法 */
                            _talkBar.find('.input-area textarea').blur();
                        });
                        /* 设置ReceiveTip事件 */
                        _page.find('.receive-tip').click(function() {
                            $main.unReadCount = 0;
                            $main.scrollToChatBottom();
                        });
                        /* 用户进入频道 */
                        app.$user.channel(channelId).done(function() {
                            /* 修正频道标题 */
                            $navBar.title = app.$channels[channelId].detail.name;
                            /* 绑定接收器 */
                            app.$wsAcceptor.callback(CONST.WS_ELE_TYPE_TALK, function(msgEle) {
                                $main.$store.dispatch(CONST.ACT_RECEIVE, msgEle);
                            });
                        }).fail(function() {
                            console.log('进入频道失败');
                        });
                        /* 初始化聊天 */
                        this.$store.dispatch(CONST.ACT_INIT_CHAT, channelId);
                        /* 显示页面 */
                        $(this.$el).show();

                        return $main;
                    },
                    fetchHistory: function(limitVal) {
                        if (this.refreshDown) return;
                        var $def = $.Deferred();
                        app.GET('/chat/history', {type: CONST.TYPE_CHANNEL, type_id: channelId, limit_val: limitVal}).done(function(data) {
                            if (data.logs && data.logs.length) {
                                $main.limitVal = data.limitVal;
                                $def.resolve(data.logs);
                            }
                            if (data.end) {
                                $main.refreshDone = true;
                            }
                        }).always(function() {
                            app.$f7.pullToRefreshDone();
                            $main.refresh = false;
                        });

                        return $def;
                    },
                    pushChatList: function(chatList, msgEle) {
                        /* 判断是否需要插入时间 */
                        var last;
                        if (chatList.length) {
                            last = chatList[chatList.length- 1];
                            if (last.type != CONST.TALK_ELE_TYPE_TIME) {
                                /* 如果与上一条聊天时间差超过1分钟, 插入时间, 且必须显示昵称 */
                                if (parseInt(msgEle.time) - parseInt(last.time) > 60) {
                                    last = app.makeTalkEle({
                                        type    : CONST.TALK_ELE_TYPE_TIME,
                                        time    : msgEle.time,
                                        timeDesc: msgEle.timeDesc,
                                        loop    : msgEle.loop
                                    });
                                    chatList.push(last);
                                }
                            }
                        } else {
                            last = app.makeTalkEle({
                                type    : CONST.TALK_ELE_TYPE_TIME,
                                time    : msgEle.time,
                                timeDesc: msgEle.timeDesc,
                                loop    : msgEle.loop
                            });
                            chatList.push(last);
                        }

                        var params = {
                            userId  : msgEle.userId,
                            username: msgEle.username,
                            avatar  : msgEle.avatar,
                            content : msgEle.content,
                            time    : msgEle.time,
                            isSame  : true
                        };

                        /* 如果上一条聊天是同一个用户, 省略昵称显示 */
                        if (!chatList.length || last.type == CONST.TALK_ELE_TYPE_TIME || last.user.id!=msgEle.userId) {
                            params.isSame = false;
                        }
                        chatList.push(app.makeTalkEle(params));
                    },
                    resizeChatBoxHeight: function() {
                        _chatBox.css('minHeight', _pageContent.height()+1+'px');
                    },
                    scrollToChatBottom: function() {
                        Vue.nextTick(function() {
                            if (_chatBoard.outerHeight() > _pageContent.height()) {
                                _chatBoard.find('.talk-ele:last').velocity('scroll', {duration: 1000, container: _pageContent});
                            }
                        });
                    },
                    onBack: function() {
                        console.log('back...');
                        this.scrollToChatBottom();
                    }
                }
            });

            return $main;
        };
    }

    return (new ChatIndex()).export();
});