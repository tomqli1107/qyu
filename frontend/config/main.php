<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                    => 'app-frontend',
    'basePath'              => dirname(__DIR__),
    'defaultRoute'          => 'home',
    'bootstrap'             => ['log'],
    'controllerNamespace'   => 'frontend\controllers',
    'components'            => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass'     => 'common\models\User',
            'enableAutoLogin'   => true,
            'identityCookie'    => ['name' => '_identity-frontend', 'httpOnly' => true],
            'loginUrl'          => null,
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            //'class' => 'yii\web\CacheSession',
            'class' => 'yii\web\Session',
            'name'  => '__mqyu',
        ],
        'errorHandler' => [
            'errorAction' => 'error/index',
        ],
        'urlManager' => [
            'enablePrettyUrl'   => true,
            'showScriptName'    => false,
            'rules'             => [

            ],
        ],
        
    ],
    'params' => $params,
];
