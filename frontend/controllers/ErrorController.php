<?php
namespace frontend\controllers;

use common\components\C;
use Yii;
use frontend\components\Controller;
use yii\web\ForbiddenHttpException;

class ErrorController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();
        array_unshift($behaviors['access']['rules'], [
            'actions'   => ['index'],
            'allow'     => true,
            'roles'     => ['?'],
        ]);

        return $behaviors;
    }

    public function actionIndex() {
        $exception  = Yii::$app->getErrorHandler()->exception;
        $code       = C::CODE_ERROR;
        if ($exception instanceof ForbiddenHttpException) {
            $code = C::CODE_AUTH;
        }

        return $this->fail($code, $exception->getMessage());
    }
}