<?php
namespace frontend\controllers;

use Yii;
use common\components\Avatar;
use common\components\C;
use common\models\User;
use frontend\components\Controller;
use frontend\forms\Login as LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function behaviors() {
        $behaviors = parent::behaviors();
        array_unshift($behaviors['access']['rules'], [
            'actions'   => ['startup', 'login'],
            'allow'     => true,
            'roles'     => ['?'],
        ]);

        return $behaviors;
    }

    /**
     * 启动时的请求
     */
    public function actionStartup() {
        /* 初始化用户的状态 */
        if (Yii::$app->user->isGuest) {
            return $this->succ(['id' => User::getVisitorId(), 'avatar' => C::DEFAULT_AVATAR, 'isLogin' => false]);
        } else {
            $user = Yii::$app->user->getIdentity();
            return $this->succ([
                'id'        => $user->id,
                'username'  => $user->username,
                'phone'     => $user->phone,
                'avatar'    => Avatar::thumb($user->avatar),
                'sex'       => $user->sex,
                'birthday'  => $user->birthday ? date('Y-m-d', $user->birthday) : '',
                'isLogin'   => true]);
        }
    }

    /**
     * 登录操作
     */
    public function actionLogin() {
        if (Yii::$app->request->isPost) {
            if (Yii::$app->user->isGuest) {
                $form = new LoginForm(Yii::$app->request->post());
                list($ok, $fRe) = $form->login();
                if ($ok) {
                    return $this->succ([
                        'id'        => $fRe->id,
                        'username'  => $fRe->username,
                        'phone'     => $fRe->phone,
                        'avatar'    => $fRe->avatar,
                        'isLogin'   => true
                    ]);
                } else {
                    return $this->fail($fRe, $form->getErrorMsg());
                }
            } else {
                $user = Yii::$app->user;
                return $this->succ([
                    'id'        => $user->id,
                    'username'  => $user->username,
                    'phone'     => $user->phone,
                    'avatar'    => $user->avatar,
                    'isLogin'   => true
                ]);
            }
        }

        return '';
    }

    /**
     * 登出操作
     */
    public function actionLogout() {
        if (Yii::$app->request->isPost) {
            Yii::$app->user->logout();
            return $this->succ(['id' => User::getVisitorId(), 'isLogin' => false]);
        }

        return '';
    }
}
