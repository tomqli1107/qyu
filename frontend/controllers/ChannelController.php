<?php
/**
 * Created by PhpStorm.
 * User: tom
 * Date: 2017/5/15
 * Time: 16:20
 */

namespace frontend\controllers;

use frontend\components\Controller;
use common\models\Channel;
use common\models\Server;


class ChannelController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();
        array_unshift($behaviors['access']['rules'], [
            'actions'   => ['detail'],
            'allow'     => true,
            'roles'     => ['?'],
        ]);

        return $behaviors;
    }

    public function actionDetail($id = 0) {
        $dtl = [];
        if ($mChannel = Channel::find()->where(['id' => $id])->select(['id', 'name', 'server_id'])->asArray()->one()) {
            $mServer = Server::find()
                        ->where(['id' => $mChannel['server_id']])
                        ->select(['id', 'name', 'type', 'host', 'port'])
                        ->asArray()
                        ->one();
            $dtl = [
                'id'    => intval($mChannel['id']),
                'name'  => $mChannel['name'],
                'server'=> [
                    'id'    => intval($mChannel['server_id']),
                    'url'   => "{$mServer['host']}:{$mServer['port']}",
                    'name'  => $mServer['name'],
                    'host'  => $mServer['host'],
                    'port'  => $mServer['port'],
                ],
            ];
        }

        return $this->succ($dtl);
    }
}