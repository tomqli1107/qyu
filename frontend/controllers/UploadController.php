<?php
namespace frontend\controllers;

use common\models\User;
use Yii;
use frontend\components\Controller;

class UploadController extends Controller {

    public function actionAvatar() {
        if (!Yii::$app->request->isPost) return false;

        $data = Yii::$app->request->post();
        if ($result = Yii::$app->aliyunOSS->uploadAvatar($data['name'], $data['avatar'])) {
            if ($newUrl = $result['info']['url']) {
                $user   = Yii::$app->user->getIdentity();
                $userId = $user['id'];
                $modUser= User::findOne($userId);
                $oldUrl = $modUser->avatar;
                $modUser->avatar        = $newUrl;
                $modUser->update_time   = time();
                if ($modUser->save()) {
                    if ($oldUrl) {
                        /* 更新成功后删除旧头像 */
                        @Yii::$app->aliyunOSS->deleteAvatar($oldUrl);
                    }

                    return $this->succ($newUrl);
                } else {
                    /* 更新失败, 删除新头像 */
                    @Yii::$app->aliyunOSS->deleteAvatar($newUrl);
                }
            }
        }

        return $this->fail();
    }
}