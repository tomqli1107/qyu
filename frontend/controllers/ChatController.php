<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use frontend\posts\ChatLog as ChatLogPost;

class ChatController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();
        array_unshift($behaviors['access']['rules'], [
            'actions'   => ['log', 'history', 'members'],
            'allow'     => true,
            'roles'     => ['?'],
        ]);

        return $behaviors;
    }

    /**
     * 写入聊天记录
     */
    public function actionLog() {
        if (Yii::$app->request->isPost) {
            $chatLog = new ChatLogPost(Yii::$app->request->post());
            list($ok, $ignore) = $chatLog->log();
            if ($ok) {
                return $this->succ();
            } else {
                return $this->fail();
            }
        }

        return '';
    }

    /**
     * 历史聊天记录
     */
    public function actionHistory() {
        $rData      = Yii::$app->request->get();
        $type       = intval($rData['type']);
        $typeId     = intval($rData['type_id']);
        $limitVal   = intval($rData['limit_val']);
        list($logs, $limitVal) = Yii::$app->chatLog->history($type, $typeId, $limitVal);

        return $this->succ(['logs' => $logs, 'limitVal' => $limitVal, 'end' => $limitVal?true:false]);
    }

    /**
     * 聊天成员
     */
    public function actionMembers($type = 0, $type_id = 0) {
        $type = 1;
        $type_id = 3;
        return $this->succ(Yii::$app->chatLog->members($type, $type_id));
    }

}