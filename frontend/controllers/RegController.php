<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use frontend\forms\RegPhone as RegPhoneForm;

class RegController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();
        array_unshift($behaviors['access']['rules'], [
            'actions'   => ['phone'],
            'allow'     => true,
            'roles'     => ['?'],
        ]);

        return $behaviors;
    }

    public function actionPhone() {
        if (!Yii::$app->request->isPost) return false;

        $form = new RegPhoneForm(Yii::$app->request->post());
        list($ok, $fRe) = $form->deal();
        if ($ok) {
            return $this->succ([
                'id'        => $fRe->id,
                'username'  => $fRe->username,
                'phone'     => $fRe->phone,
                'avatar'    => $fRe->avatar,
                'isLogin'   => true
            ]);
        } else {
            return $this->fail($fRe, $form->getErrorMsg());
        }
    }
}