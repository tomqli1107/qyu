<?php
namespace frontend\controllers;

use common\components\Avatar;
use common\components\C;
use common\components\Image;
use common\models\Visitor;
use frontend\forms\UserEdit as UserEditForm;
use Yii;
use common\models\User;
use frontend\components\Controller;

class UserController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();
        array_unshift($behaviors['access']['rules'], [
            'actions'   => ['detail'],
            'allow'     => true,
            'roles'     => ['?'],
        ]);

        return $behaviors;
    }

    public function actionDetail() {

        if ($user = Yii::$app->user->getIdentity()) {
            $data   = [
                'id'        => $user['id'],
                'username'  => $user['username'],
                'avatar'    => Avatar::thumb($user['avatar'], Image::STYLE_SMALLSQUARE),
                'phone'     => $user['phone'],
                'sex'       => intval($user['sex']),
                'time'      => date('Y-m-d', $user['create_time']),
            ];
        } else {
            $detail = Visitor::detail();
            $data   = [
                'id'        => $detail['id'],
                'username'  => $detail['name'],
                'avatar'    => Avatar::thumb(),
                'phone'     => '',
                'sex'       => 0,
                'time'      => $detail['createTime'],
            ];
        }

        return $this->succ($data);
    }

    public function actionEdit() {
        if (!Yii::$app->request->isPost) return false;

        $form = new UserEditForm(Yii::$app->request->post());
        list($ok, $ignore) = $form->deal();
        if ($ok) {
            return $this->succ();
        } else {
            return $this->fail();
        }
    }
}