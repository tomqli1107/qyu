<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use frontend\forms\Search as SearchForm;


class SearchController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();
        array_unshift($behaviors['access']['rules'], [
            'actions'   => ['index'],
            'allow'     => true,
            'roles'     => ['?'],
        ]);

        return $behaviors;
    }

    public function actionIndex() {
        if (Yii::$app->request->isPost) {
            $form = new SearchForm(Yii::$app->request->post());
            list($ok, $fRe) = $form->deal();
            if ($ok) {
                return $this->succ(['channelId' => $fRe]);
            } else {
                return $this->fail($fRe, $form->getErrorMsg());
            }
        }
    }
}