<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use frontend\forms\SmsCode as SmsCodeForm;

class PhoneController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();
        array_unshift($behaviors['access']['rules'], [
            'actions'   => ['send-sms-code'],
            'allow'     => true,
            'roles'     => ['?'],
        ]);

        return $behaviors;
    }

    public function actionSendSmsCode() {
        if (Yii::$app->request->isPost) {
            $form = new SmsCodeForm(Yii::$app->request->post());
            list($ok, $fRe) = $form->deal();
            if ($ok) {
                return $this->succ();
            } else {
                return $this->fail($fRe, $form->getErrorMsg());
            }
        }

        return '';
    }
}