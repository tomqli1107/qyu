<?php

namespace frontend\forms;

use Yii;
use common\components\C;
use common\validators\PhoneNum;
use frontend\components\Form;

class SmsCode extends Form {

    public $phone_num;
    public $type;

    public function rules() {
        return [
            ['phone_num', PhoneNum::className()],
            ['type', 'number', 'message' => '错误的类型'],
        ];
    }

    public function validatePhoneNum($attribute, $params) {
        $this->phone_num = trim($this->phone_num);
        if (!preg_match('/^1[0-9]{10}$/', $this->phone_num)) {
            $this->addError($attribute, '无效的手机号码');
        }
    }

    public function deal() {
        if (!$this->validate()) return [false, C::CODE_VALID];

        if (Yii::$app->sms->publish(C::SMS_TYPE_CODE, [$this->type, $this->phone_num])) {
            return [true, null];
        }

        return [false, C::CODE_ERROR];
    }
}