<?php

namespace frontend\forms;

use common\components\C;
use common\validators\PhoneNum;
use common\validators\SmsCode;
use common\validators\Password;
use common\models\User;
use frontend\components\Form;

class Login extends Form {

    public $phone_num;
    public $code;
    public $password;
    public $type;

    public function rules() {
        return [
            ['type', 'in', 'range' => [C::LOGIN_TYPE_PW, C::LOGIN_TYPE_CODE], 'message' => '非法登录'],
            ['phone_num', [
                'class'     => PhoneNum::className(),
                'scene'     => C::SCENE_LOGIN,
            ]],
            ['code', [
                'class'     => SmsCode::className(),
                'scene'     => C::SCENE_LOGIN,
                'phoneNum'  => $this->phone_num,
                'type'      => $this->type
            ]],
            ['password', [
                'class'     => Password::className(),
                'scene'     => C::SCENE_LOGIN,
                'type'      => $this->type,
            ]],
        ];
    }

    public function login() {
        if (!$this->validate()) return [false, C::CODE_VALID];

        /* 检测手机号是否已注册 */
        $user = User::findByPhone($this->phone_num);
        if (!$user) {
            $this->setFatal('手机号未注册');
        } else {
            /* 检测用户状态是否正常 */
            if ($user->allowLogin()) {
                switch ($this->type) {
                    case C::LOGIN_TYPE_PW:
                        /* 验证密码 */
                        if (!$user->validatePassword($this->password)) {
                            $this->setFatal('密码错误');
                        }
                        break;
                }
            } else {
                $this->setFatal('该账号暂时无法登录');
            }
        }

        if ($this->validateFatal()) {
            $user->login();
            return [true, $user];
        } else {
            return [false, C::CODE_VALID];
        }
    }
}