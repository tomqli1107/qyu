<?php

namespace frontend\forms;

use Yii;
use common\components\C;
use common\models\User;
use frontend\components\Form;

class UserEdit extends Form {

    public $username;
    public $sex;
    public $birthday;

    public function rules() {
        return [
            [['username', 'sex', 'birthday'], 'required'],
        ];
    }

    public function deal() {
        if (!$this->validate()) return [false, C::CODE_VALID];

        $data = [
            'id'        => Yii::$app->user->getId(),
            'username'  => $this->username,
            'sex'       => $this->sex,
            'birthday'  => strtotime($this->birthday),
        ];

        if (User::edit($data)) {
            return [true, Yii::$app->user->getId()];
        }

        return [false, C::CODE_FAIL];
    }
}