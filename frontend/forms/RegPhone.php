<?php

namespace frontend\forms;

use common\components\C;
use common\validators\PhoneNum;
use common\validators\SmsCode;
use common\validators\Password;
use common\models\User;
use frontend\components\Form;

class RegPhone extends Form {

    public $phone_num;
    public $code;
    public $password;

    public function rules() {
        return [
            ['phone_num', ['class' => PhoneNum::className(), 'scene' => C::SCENE_REG]],
            ['code', [
                'class'     => SmsCode::className(),
                'scene'     => C::SCENE_REG,
                'phoneNum'  => $this->phone_num,
            ]],
            ['password', [
                'class'     => Password::className(),
                'scene'     => C::SCENE_LOGIN,
            ]],
        ];
    }

    public function deal() {
        if (!$this->validate()) return [false, C::CODE_VALID];

        list($ok, $user) = User::newByPhone($this->phone_num, $this->password);
        if ($ok) {
            /* 注册成功后自动登录 */
            /* @var \common\models\User $user */
            $user->login();
        }

        return [$ok, $user];
    }
}