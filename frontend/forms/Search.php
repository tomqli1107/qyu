<?php

namespace frontend\forms;

use common\models\Channel;
use frontend\components\Form;
use common\components\C;

class Search extends Form {

    public $input;
    public $type;

    public function rules() {
        return [
            ['input', 'required', 'message' => '请输入内容'],
            ['type', 'number', 'message' => '错误的搜索类型'],
        ];
    }

    public function deal() {
        if (!$this->validate()) return [false, C::CODE_VALID];

        switch ($this->type) {
            case C::TYPE_CHANNEL:
                /* 返回一个频道id */
                if ($channelId = Channel::search($this->input))
                    return [true, $channelId];
                break;
        }

        return [false, C::CODE_ERROR];
    }
}