<?php

namespace frontend\components;

use yii\filters\AccessControl;

class Controller extends \common\components\Controller {

    public function init() {
        parent::init();
    }

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        //'matchCallback' => [$this, 'validateUserStatus']
                    ],
                ],
            ],
        ];
    }
}