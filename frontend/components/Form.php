<?php

namespace frontend\components;

use yii\base\Model;

class Form extends Model {

    protected $fatalMsg;

    public function __construct($data) {
        parent::__construct();
        if ($data) {
            foreach ($data as $field => $val) {
                if (property_exists($this, $field)) {
                    $this->{$field} = $val;
                }
            }
        }
    }

    public function getErrorMsg() {
        if ($this->fatalMsg) $error = $this->fatalMsg;
        else {
            if ($error = $this->getFirstErrors()) {
                $error = array_values($error)[0];
            }
        }

        return $error ?: '';
    }

    public function setFatal($msg) {
        $this->fatalMsg = $msg;
    }

    public function validateFatal() {
        return !$this->fatalMsg;
    }

}