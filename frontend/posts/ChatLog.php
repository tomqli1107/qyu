<?php

namespace frontend\posts;

use Yii;
use common\components\C;
use frontend\components\Form;

class ChatLog extends Form {

    public $user_id;
    public $visitor;
    public $content;
    public $type_id;
    public $type;

    public function rules() {
        return [
            ['type', 'in', 'range' => [C::TYPE_CHANNEL], 'message' => '错误的类型'],
            [['user_id', 'type_id'], 'match', 'pattern' => '/^[1-9]\d*$/', 'message' => 'id值格式错误'],
            ['content', 'required', 'message' => '聊天内容为空'],
        ];
    }

    public function log() {
        if (!$this->validate()) return [false, C::CODE_VALID];

        $user   = Yii::$app->user->getIdentity();
        $userId = $user['id'];

        /* @var \common\components\ChatLog $chatLog */
        $chatLog = Yii::$app->chatLog;
        if ($chatLog->log([
            'user_id'   => $userId ?: ($this->user_id?:0),
            'visitor'   => $userId ? 0 : ($this->visitor?1:0),
            'type'      => $this->type,
            'type_id'   => $this->type_id,
            'content'   => $this->content,
        ])) {
            return [true, null];
        }

        return [false, C::CODE_FAIL];
    }
}